phalcon-personalwork
=============

Customize Phalcon Framework by Personalwork
-------------
## Infomation
[0.8.6]
1. 新增加Personalwork/Exception主類別
	- 以身份驗證流程過程可能發生錯誤延伸例外事件


[0.8.3]
1. 新增Annotations/LoadAssets.php
	- 提供特定專案plugins/LoadAssets.php繼承，處理透過Annotations機制來載入bower或其他第三方js套件檔案清單。

[0.7.3]
1. 新增Forms/Builder.php
	- 可直接指定ModelClass產生對應表單類別並指定產生路徑。
2. 修正Forms/Elements類別，擴增原Phalcon/Forms缺少的元素類別，附加預設render方法產出基礎結構化html
	- 新增Numeric元素
	- 新增Date元素

[0.7.0dev]
1. 調整Mvc/Controller/Base/Application.php
	- 調整$this->assets->collection對應命名與config.php內同步。
	- 調整判斷$item['url']避免Notice錯誤狀況。
2. 從專案*pkerp*版本獨立後持續更新後繼版本改用0.7.0版以上。

[0.6.7dev]
1. 加入Package/Curl 函式庫處理開發機器人程式所需函式。
	- /Package/Http.php
	- /Package/Multimedia.php
	- /Package/Parser.php

[0.6.5dev]
1. 第一版群組表單欄位配置流程，透過以下檔案
	- ### [project] config/xxx.json
		> 在個別專案config目錄夾內配置json格式檔，詳細結構說明可以參考 pkerp專案的core.json
	- ### Forms/FormGroups.php
		> 擴充$form物件屬性，新增 $elementsGroup 儲存elements group，並透過Array結構紀錄elements資訊與數量
	- ### Mvc/View/Helper/FormGroups.php
		> 制定樣式顯示method套用規則 參考 _gen_fieldset()
