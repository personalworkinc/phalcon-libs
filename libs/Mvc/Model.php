<?php
/**
 * Model機制
 *
 * 擴充功能
 * 1. 新增find($paramesters)可以接受陣列結構作為IN()篩選條件
 * */

namespace Personalwork\Mvc;

class Model extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
    }

    public function afterFetch()
    {
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BindingaccountExtendinfo[]
     */
    public static function find($parameters = null)
    {
        return parent::find(self::_prepareBinds($parameters));
    }


    /*
     * FROM:
     * 0 => string 'id IN (:ids:)' (length=13)
     * 'bind' =>
     *   array (size=1)
     *     'ids' =>
     *       array (size=4)
     *         0 => int 15
     *         1 => int 17
     *         2 => int 18
     *         3 => int 19
     * 'for_update' => boolean true
     *
     * TO:
     * 0 => &string 'id IN (?0, ?1, ?2, ?3)' (length=22)
     * 'bind' =>
     *   array (size=4)
     *     0 => int 15
     *     1 => int 17
     *     2 => int 18
     *     3 => int 19
     * 'for_update' => boolean true
     */
    private static function _prepareBinds($parameters = NULL)
    {
        // binding is allowed only in array
        if (!is_array($parameters))
            return $parameters;

        // getting conditions from 0 or conditions parameter
        if (!empty($parameters['conditions']))
            $conditions =& $parameters['conditions'];
        elseif (!empty($parameters[0]))
            $conditions =& $parameters[0];
        else
            $conditions = '';

        // finding largest already set placeholder to avoid conflicts
        if (preg_match('/.*\?(\d+)/', $conditions, $matches))
            $i = $matches[1] + 1;
        else
            $i = 0;

        /*
         * check if exists bind and replace all arrays to ?0 ?1 etc
         */
        if (!empty($parameters['bind']))
            foreach ($parameters['bind'] as $key => $binded)
                if (is_array($binded))
                {
                    $placeholders = array();
                    $binds = array();
                    foreach ($binded as $bind)
                    {
                        $placeholders[] = '?'.$i;
                        $parameters['bind'][$i] = $bind;
                        $i++;
                    }
                    unset($parameters['bind'][$key]);
                    $conditions = str_replace(':'.$key.':', implode(', ', $placeholders), $conditions);
                }
        return $parameters;
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BindingaccountExtendinfo
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
