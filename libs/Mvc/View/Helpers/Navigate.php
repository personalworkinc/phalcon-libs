<?php
/**
 * 建立 Nagivation 選單區塊樣式
 * 
 * @package Mvc\View
 * @subpackage Helpers
 * @author San Haung <san@mail.personalwork.tw>
 * @version 2014122101
 * 1. 於 toHtml 內加入判斷登入後顯示登出按鈕機制
 */
 
/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 * 
 */
namespace Personalwork\Mvc\View\Helpers;

class Navigate {

    /**
     * Html 
     * 
     * @var string 
     */
    protected $html = '';

    /**
     * Translate
     * 
     * @var type 
     */
    protected $t;

    /**
     * @todo add acl veryfication
     * @todo add schedule veryfication
     * 
     */
    public function __construct() {
        if(is_null($this->t) && \Phalcon\DI::getDefault()->has('translate'))
            $this->t = \Phalcon\DI::getDefault()->get('translate');
    }
    
    /**
     * Translate proxy
     * 
     * @param type $word
     * @return string
     */
    private function _translate($word)
    {
        if(is_null($this->t))
            return $word;
        
        return $this->t->_($word);
    }
    
    /**
     * Create ul elements
     * 
     * @param type $node
     */
    private function _generate($node) {
        $class = !is_null($node->getClass()) ? " class='" . $node->getClass() . "' " : '';
        $id = !is_null($node->getId()) ? " id='" . $node->getId() . "'" : '';

        $this->html .= "\t<ul$class$id>" . PHP_EOL;
        if ($node->hasChilds())
            $this->_generateChilds($node->getChilds());
       
        //判斷登入狀態
        if( \Phalcon\DI::getDefault()->has('auth') ){
            $auth = \Phalcon\DI::getDefault()->get('auth');
            $user = $auth->getIdentity();
            $this->html .= '<li><a title="登出" href="/people/logout">登出</a></li>';
        }
       
        $this->html .= "\t</ul>" . PHP_EOL;
    }

    /**
     * Create childs element
     * 
     * @param type $childs
     */
    private function _generateChilds($childs) {
        foreach ($childs as $node) {
            $this->_generateElement($node);
        }
    }

    /**
     * Create one element
     * 
     * @param type $node
     */
    private function _generateElement($node) {
        $cssClasses = array();
        if ($node->isActive())
            $cssClasses[] = 'active';

        if (!is_null($node->getClass()))
            $cssClasses[] = $node->getClass();

        $class = count($cssClasses) > 0 ? " class='" . implode(',', $cssClasses) . "'" : '';
        $id = !is_null($node->getId()) ? " id='" . $node->getId() . "'" : '';
        $target = !is_null($node->getTarget()) ? " target='" . $node->getTarget() . "'" : '';
        
        $toggle = ($node->hasChilds()) ? 'class="dropdown-toggle"' : '';
        $toggleappend = ($node->hasChilds()) ? '<b class="caret"></b>' : '';
        
        
        //add icon attribute
        $icon = !is_null($node->getIcon()) ? "<i class='" . $node->getIcon() . "'></i>" : '';
        
        $this->html .= "\t\t<li$class $id>" . PHP_EOL;
        $this->html .= "\t\t\t<a title='". $this->_translate($node->getName()) . "' href='" . $node->getUrl() . "' $target $toggle>". $icon . $this->_translate($node->getName()) . $toggleappend . "</a>" . PHP_EOL;

        //generate childs
        if ($node->hasChilds()) {
            $this->html .= "\t\t<ul class='dropdown-menu'>" . PHP_EOL;
            $this->_generateChilds($node->getChilds());
            $this->html .= "\t\t</ul>" . PHP_EOL;
        }

        $this->html .= "\t\t</li>" . PHP_EOL;
    }


    private function _generateAside($node){

        $this->html .= "\t<ul id='dashboard-menu' class='nav nav-list'>" . PHP_EOL;
        
        if ($node->hasChilds()){
        foreach ($node->getChilds() as $node) {
            $cssClasses = array();
            if ($node->isActive())
                $cssClasses[] = 'active';
    
            if (!is_null($node->getClass()))
                $cssClasses[] = $node->getClass();
    
            $class = count($cssClasses) > 0 ? " class='" . implode(',', $cssClasses) . "'" : '';
            $id = !is_null($node->getId()) ? " id='" . $node->getId() . "'" : '';
            
            //add icon attribute
            $icon = !is_null($node->getIcon()) ? "<i class='" . $node->getIcon() . "'></i>" : '';
            
            $this->html .= "\t\t<li$class $id>" . PHP_EOL;
            $this->html .= "\t\t\t<a title='". $this->_translate($node->getName()) . "' href='" . $node->getUrl() . "'>". $icon . $this->_translate($node->getName()) . "</a>" . PHP_EOL;
    
            //generate childs
            if ($node->hasChilds()) {
                $this->html .= "\t\t<div class='submenu'>" . PHP_EOL;
                $this->html .= "\t\t<ul>" . PHP_EOL;
                $this->_generateChilds($node->getChilds());
                $this->html .= "\t\t</ul>" . PHP_EOL;
                $this->html .= "\t\t</div>" . PHP_EOL;
            }
    
            $this->html .= "\t\t</li>" . PHP_EOL;
        }
        }
        
        $this->html .= "\t</ul>" . PHP_EOL;
    }

    /**
     * Generate all HTML
     * 
     * @param type $node
     * @return string
     */
    public function toHtml($node, $format) {
        if( $format == 'bar' ){
            $this->_generate($node);
        } elseif( $format == 'aside' ) {
            $this->_generateAside($node);
        }
        return $this->html;
    }

}
