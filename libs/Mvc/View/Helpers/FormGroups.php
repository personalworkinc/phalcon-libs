<?php

/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 * 
 */

namespace Personalwork\Mvc\View\Helpers;

use \Phalcon\Tag as Tag;

class FormGroups {

    /**
     *
     * @var string 
     */
    public $html = '';


    /**
     * patent::__construct()
     */
    public function __construct(\Personalwork\Forms\Form $form) {
        $this->form = $form;
    }

    /**
     * 基本版本單純生成 <fieldset> 形式 
     */
    private function _gen_fieldset() {
        $groupelms = $this->form->getFieldSet();
        
        foreach ($groupelms as $index => $set) {
            
            $this->html.= "\t\t".Tag::tagHtml('fieldset',array('class'=>'form-fieldset'), FALSE, TRUE, TRUE);

            $this->generateElements( (array)$set->elements );
            
            $this->html.= Tag::tagHtmlClose('fieldset').PHP_EOL;
        }
    }
    
    /**
     * (Override from Personalwork\Mvc\View\Helpers\Form )
     * @param type $name
     * @return type
     */
    protected function getElementType($name)
    {
        $tab = explode("\\",$name);
        return end($tab);
        unset($tab);
    }
    
    protected function generateElements( $elms ) {
        
        //根據配置參數動態建立表單
        $elements = $this->form->findElementByKey( $elms );
        
        //處理根據element配置的type顯示對應的Decorator(可根據elements附加設定鍵值來決定Decorator)
        foreach( $elements as $element ){
            
            //element doesn't have any decorator
            if(!isset($element->decorator)){
                $type = $this->getElementType(get_class($element));
                //create default decorator
                $className = '\\Personalwork\\Forms\\Decorators\\'.$type;
                
                if(!class_exists($className))
                    throw new \Exception("$className doesn't exists");
                
                $class = new $className();
                $class->setElement($element);
                $this->html.= $class->toHtml().PHP_EOL;
                
            }elseif( class_exists($element->decorator) ){
                $className = $element->decorator;
                $class = new $className();
                $class->setElement($element);
                $this->html.= $class->toHtml().PHP_EOL;
            }else{
                throw new \Exception(get_class($element). "doesn't have decorator");
            }
        }
        
    }

    /**
     * Generate all HTML 
     * ( referenced from Form::toHtml() )
     * 
     * @param \Modules\Form $form
     * @return string
     */
    public function toHtml( $mode='fieldset' )
    {
        $formParams = array(
            'action' => $this->form->getAction(),
            'method' => $this->form->getMethod(),
        );

        if (!is_null($this->form->getCssClass()))
            $formParams['class'] = $this->form->getCssClass();

        if (!is_null($this->form->getCssId()))
            $formParams['id'] = $this->form->getCssId();

        if (!is_null($this->form->getEnctype()))
            $formParams['enctype'] = $this->form->getEnctype();

        $this->html .= "\t".Tag::form(array_merge($formParams, $this->form->getCustomTags())).PHP_EOL;

        $this->{"_gen_".$mode}();
        
        //generate submit button
        $this->generateElements('send');

        $this->html .= "\t".Tag::endForm().PHP_EOL;

        return $this->html;
    }

}
