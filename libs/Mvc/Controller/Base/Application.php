<?php

namespace Personalwork\Mvc\Controller\Base;

use Phalcon\Mvc\Controller;

class Application extends Controller
{
    public function initialize()
    {
        if( $this->getDI()->has('navigation') ){
            $this->getDI()->get('navigation')->setActiveNode(
                $this->router->getActionName(),
                $this->router->getControllerName(),
                $this->router->getModuleName()
            );

            $this->view->navigation = $this->getDI()->get('navigation');
        }

        $this->tag->setDoctype(\Phalcon\Tag::HTML5);

        if( $this->getDI()->has('config') ){
            $this->tag->setTitle( $this->config->personalwork->projectname );

            $misc = $this->config->misc;
            if( isset($misc->headerStyle) ){
                foreach($misc->headerStyle as $cssfile => $css){
                    if( is_string($css) ){
                        $this->assets->collection('headerStyle')->addCss($css, true);
                    }elseif( count($css->toArray()) > 0 ){
                        foreach ($css as $sourcecss => $attrs) {
                        $this->assets->collection('headerStyle')
                                     ->setAttributes($css->toArray())
                                     ->addCss($cssfile, true);
                        }
                    }
                }
            }

            if( isset($misc->headerScript) ){
                foreach($misc->headerScript as $jsfile => $script){
                    if( is_string($script) ){
                        $this->assets->collection('headerScript')->addJs($script, true);
                    }elseif( count($script->toArray()) > 0 ){
                        $this->assets->collection('headerScript')
                                     ->setAttributes($script->toArray())
                                     ->addJs($jsfile, true);
                    }
                }
            }

            if( isset($misc->footerScript) ){
                foreach($misc->footerScript as $jsfile => $script){
                    if( is_string($script) ){
                        $this->assets->collection('footerScript')->addJs($script, true);
                    }elseif( count($script->toArray()) > 0 ){
                        foreach ($script as $sourcejs => $attrs) {
                        $this->assets->collection('footerScript')
                                     ->setAttributes($script->toArray())
                                     ->addJs($jsfile, true);
                        }
                    }

                }
            }
        }

        /**
         * 處理prepost紀錄前一次表單post完整內容以便可以往前檢視與避免重複處理一樣的資料。
         */
        if( $this->session->has('prepost') ){
            $prepost = unserialize( $this->session->get('prepost') );
            if( $prepost == $_POST ){
                $this->REPOST = true;

                if( !$this->request->isAjax() ){
                    // $this->flashSession->notice("系統已處理過本次表單，將直接取出已存在資料顯示。");
                }
            }

            if( $this->request->isPost() ){
                $this->session->set('prepost', serialize($_POST) );
            }
        }elseif( $this->request->isPost() ){
            $this->session->set('prepost', serialize($_POST));
        }

        //[pkerp]
        $this->params = explode("-", trim($this->dispatcher->getParam('param'),'/'));
    }


    /**
     * 處理動態配置選單麵包屑路徑
     * 1.結構：name icon class link or active
     * 2.直接透過\Personalwork\Navigation::getNavConfigure()取出navigation.json檔內配置的選單路徑
     * 來判斷目前active項目。
     */
    private function _recursive($item, $path=null) {
        if( isset($item['url']) && $item['url'] == $this->router->getRewriteUri() or
            // 斜線前置反斜線
            ( isset($item['url-regx']) and preg_match("/{$item['url-regx']}/", $this->router->getRewriteUri()) ) ){
            $path[] = array('icon'=>$item['icon'],'class'=>$item['class'],'name'=>$item['name'],'url'=>$item['url'],'active'=>true);
            $this->view->BREADCRUMB = $path;
        }elseif( !empty($item['childs']) ){
            if( isset($item['class']) && $item['class'] != 'main-menu'){
                $path[] = array('icon'=>$item['icon'],'class'=>$item['class'],'name'=>$item['name'],'url'=>$item['url']);
            }
            foreach($item['childs'] as $item2){
                if( empty($this->view->BREADCRUMB) ){
                    $this->_recursive($item2, $path);
                }
            }
        }
    }


    public function beforeExecuteRoute( $dispatcher )
    {
        if( !$this->request->isAjax() && $this->getDI()->has('navigation') ){
            // initialize breadcrumb
            $nav = $this->getDI()->get('navigation')->getNavConfigure('main')->toArray();
            $this->view->BREADCRUMB=null;
            $this->_recursive($nav);
        }
    }

    public function afterExecuteRoute( $dispatcher )
    {
        // 處理prepost應用後移除原本配置
        if( isset($this->view->prepost) ){
            $this->session->remove('prepost');
        }

        // 配置<title>
        if( isset($this->view->PAGETITLE) ){
            $this->tag->prependTitle("{$this->view->PAGETITLE} - ");
        }
    }
}
