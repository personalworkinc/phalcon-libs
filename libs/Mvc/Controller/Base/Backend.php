<?php

namespace Personalwork\Mvc\Controller\Base;

use Phalcon\Mvc\Controller;

class Backend extends Controller
{
    public function initialize()
    {
        $this->getDI()->get('backendnav')->setActiveNode(
            $this->router->getActionName(),
            $this->router->getControllerName(),
            $this->router->getModuleName()
        );
        
        $this->view->navigation = $this->getDI()->get('backendnav');
        
        $this->assets
             //->addCss('//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css', false)
             ->addCss('jplug/bootstrap23/css/bootstrap.css', true)
             ->addCss('jplug/fontawesome42/css/font-awesome.css', true)
             ->addCss('misc/layouts-backend.css', true);
        
        $this->assets
             ->collection('header')
             ->addJs('misc/jquery-1.11.1.min.js')
             ->addJs('jplug/bootstrap32/js/bootstrap.min.js')
             ->addJs('misc/jquery-ui.min.js')
             ->addJs('misc/jqBootstrapValidation.js');
             
        $this->params = explode("-", $this->dispatcher->getParam('param'));
        
        //針對paginator配置連結網址
        if( preg_match("/page=\d+/", $_SERVER['REQUEST_URI']) ){
            $this->view->currentPageUrl =  preg_replace("/page=\d+/", "page=", $_SERVER['REQUEST_URI']);
        }elseif( preg_match("/\?/", $_SERVER['REQUEST_URI']) ){
            $this->view->currentPageUrl =  $_SERVER['REQUEST_URI'].'&page=';
        }else{
            $this->view->currentPageUrl =  $_SERVER['REQUEST_URI'].'?page=';
        }
    }
}
