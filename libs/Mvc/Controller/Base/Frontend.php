<?php

namespace Personalwork\Mvc\Controller\Base;

use Phalcon\Mvc\Controller;

class Frontend extends Controller
{
    public function beforeExecuteRoute( $dispatcher )
    {
        //Debug::dump($dispatcher->getActionName());
        //Debug::dump($dispatcher->getControllerName());
        //die(); 
    }
    
    public function initialize()
    {
        $this->getDI()->get('frontendnav')->setActiveNode(
            $this->router->getActionName(),
            $this->router->getControllerName(),
            $this->router->getModuleName()
        );
        
        $this->view->navigation = $this->getDI()->get('frontendnav');
        
        $this->tag->setDoctype(\Phalcon\Tag::HTML5);
        
        $this->tag->prependTitle("核心反應訓練 - ");
        $this->tag->setTitle("首頁");
        
        $this->assets
             //->addCss('//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css', false)
             ->addCss('jplug/bootstrap32/css/bootstrap.css', true)
             ->addCss('jplug/fontawesome42/css/font-awesome.css', true)
             ->addCss('jplug/bootstrap-messenger/css/messenger.css', true)
             ->addCss('jplug/bootstrap-messenger/css/messenger-theme-flat.css', true)
             ->addCss('misc/layouts-frontend.css', true);
        
        $this->assets
             ->collection('header')
             ->addJs('misc/jquery-1.11.1.min.js')
             ->addJs('jplug/bootstrap32/js/bootstrap.min.js')
             ->addJs('misc/jqBootstrapValidation.js')
             ->addJs('jplug/bootstrap-messenger/js/messenger.min.js')
             ->addJs('jplug/bootstrap-messenger/js/messenger-theme-future.js');
              
              
        $this->assets
             ->collection('footer')
             ->addJs('misc/frontend.js');
    }
    
    
    public function AfterExecuteRoute( $dispatcher )
    {
        
    }
}
