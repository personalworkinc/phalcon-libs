<?php
/**
 * 錯誤例外核心機制 - 
 * 
 * 提供介面：
 * 1. route404 - 頁面找不到
 * 
 * @package PPS
 * @subpackage Core
 * @category Error
 * @author San Huang <san@mail.personalwork.tw>
 * @version 1.5
 *  
 */
  
namespace Personalwork\Mvc\Controller;
 
use Personalwork\Mvc\Controller\Base\Frontend as ControllerBase;

class CollectController extends ControllerBase
{
    
    public function initialize()
    {

    }
    
    
    public function route404Action()
    {
        $block = '<div class="row-fluid login"> Page Not Found! </div>';
        
        return $block;
    }

}

