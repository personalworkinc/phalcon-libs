<?php
/**
 * cli $phalcon model logger --namespace=Personalwork\\Mvc\\Model\\Logger --get-set --doc --force
 */

/**
 *
 * Table structure for table `logger`
 *
CREATE TABLE `logger` (
  `loggerId` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL COMMENT '訊息類型',
  `event` varchar(50) NOT NULL DEFAULT '' COMMENT '事件名稱/發生原因代碼',
  `trigger` varchar(255) NOT NULL DEFAULT '' COMMENT '觸發/動作(route or actoin or event trigger)',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '訊息',
  `setTime` int(10) unsigned NOT NULL COMMENT '記錄時間',
  `user_agent` varchar(512) DEFAULT NULL COMMENT '記錄使用裝置',
  `vars` text COMMENT '事件參數',
  PRIMARY KEY (`loggerId`)
) ENGINE=InnoDB AUTO_INCREMENT=67823 DEFAULT CHARSET=utf8;
 *
 *
 * */

namespace Personalwork\Mvc\Model;

class Logger extends \Personalwork\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $loggerId;

    /**
     *
     * @var integer
     */
    public $type;

    /**
     *
     * @var string
     */
    public $event;

    /**
     *
     * @var string
     */
    public $trigger;

    /**
     *
     * @var string
     */
    public $message;

    /**
     *
     * @var integer
     */
    public $setTime;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var string
     */
    public $vars;

    public function getSource()
    {
        return 'logger';
    }

    /**
     * 轉換特定欄位
     * rule1. 整理訊息欄位
     * rule2. 處理事件說明
     * rule3. 處理觸發說明
     */
    public function afterFetch()
    {
        $this->messageShow = str_replace("|", "", $this->message);
        $this->logTime = date('Y-m-d H時i分');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'loggerId' => 'loggerId',
            'type' => 'type',
            'event' => 'event',
            'trigger' => 'trigger',
            'message' => 'message',
            'setTime' => 'setTime',
            'user_agent' => 'user_agent',
            'vars' => 'vars'
        );
    }

}
