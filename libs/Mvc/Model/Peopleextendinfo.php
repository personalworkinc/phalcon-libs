<?php
/**
 * cli $phalcon model people_extendinfo --namespace=Personalwork\\Mvc\\Model\\PeopleExtendinfo --get-set --doc --force
 */

/**
 *
 * Table structure for table `people_extendinfo`
 *
CREATE TABLE `people_extendinfo` (
  `extendinfoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PeopleId` int(10) NOT NULL,
  `key` varchar(30) NOT NULL DEFAULT '0',
  `value` varchar(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`extendinfoId`),
  KEY `fk_people_extendinfo_people_idx` (`PeopleId`),
  CONSTRAINT `fk_people_extendinfo_peopleId` FOREIGN KEY (`PeopleId`) REFERENCES `people` (`peopleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
 *
 *
 * */

namespace Personalwork\Mvc\Model;

class PeopleExtendinfo extends \Personalwork\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $extendinfoId;

    /**
     *
     * @var integer
     */
    public $PeopleId;

    /**
     *
     * @var string
     */
    public $key;

    /**
     *
     * @var string
     */
    public $value;


    public function initialize()
    {
        $this->belongsTo("PeopleId", "\Personalwork\Mvc\Model\People", "peopleId", array('alias' => 'People'));
    }

    public function afterFetch()
    {
        if( array_key_exists($this->key, $this->keylists) ){
            $this->keyinfo = $this->keylists[$this->key];
        }
    }
}
