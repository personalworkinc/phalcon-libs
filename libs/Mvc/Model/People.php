<?php
/**
 * cli $phalcon model people --namespace=Personalwork\\Mvc\\Model\\People --get-set --doc --force
 */

/**
 *
 * Table structure for table `people`
 *
CREATE TABLE `people` (
  `peopleId` int(10) NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `account` varchar(30) NOT NULL DEFAULT '' COMMENT '帳號',
  `alias` varchar(50) DEFAULT '' COMMENT '識別碼',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '郵件信箱',
  `password` varchar(120) NOT NULL DEFAULT '' COMMENT '密碼',
  `RoleId` tinyint(2) DEFAULT NULL COMMENT '群組編號',
  `RoleName` varchar(20) DEFAULT '' COMMENT '群組名稱',
  `RoleAlias` varchar(20) DEFAULT '' COMMENT '群組識別碼',
  `statecode` tinyint(1) NOT NULL DEFAULT '0' COMMENT '最新狀態',
  `initTime` int(10) unsigned NOT NULL COMMENT '帳號註冊時間',
  `lastTime` int(10) unsigned DEFAULT NULL COMMENT '最後登入時間',
  PRIMARY KEY (`peopleId`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
 *
 *
 * */

namespace Personalwork\Mvc\Model;

use \Phalcon\Security;
use \Personalwork\Package\Pinyin;

class People extends \Personalwork\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $peopleId;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $account;

    /**
     *
     * @var string
     */
    public $alias;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var int
     */
    public $RoleId;

    /**
     *
     * @var string
     */
    public $RoleName;

    /**
     *
     * @var string
     */
    public $RoleAlias;

    /**
     *
     * @var string
     */
    public $statecode;

    /**
     *
     * @var int
     */
    public $initTime;

    /**
     *
     * @var int
     */
    public $lastTime;


    public function initialize()
    {
        $this->hasMany("peopleId", "\Personalwork\Mvc\Model\PeopleExtendinfo", "PeopleId", array('alias' => 'PeopleExtendinfo'));

        //在使用update時排除特定欄位
        $this->skipAttributesOnUpdate(array('initTime'));
        //在使用insert時排除特定欄位
        $this->skipAttributesOnCreate(array('lastTime'));
    }

    public function getSource()
    {
        return 'people';
    }

    /**
     * Returns the value of field initTime
     *
     * @return string
     */
    public function getInittime()
    {
        $this->initTime = date('Y-m-d H:i:s', $this->initTime);
        return $this->initTime;
    }


    /**
     * Returns the value of field lastTime
     *
     * @return string
     */
    public function getLasttime()
    {
        $this->lastTime = date('Y-m-d H:i:s', $this->lastTime);
        return $this->lastTime;
    }


    /**
     */
    public function beforeSave()
    {
    }


    public function beforeValidation()
    {
        $pinyin = new Pinyin();
        $this->alias = $pinyin->converter( $this->name );
    }


    //after beforeValidation
    public function beforeValidationOnCreate()
    {
        $this->initTime = time();
        if( !isset($this->statecode) )
            $this->statecode = self::STAT_INIT;

        $i = 1;
        $name = $this->name;
        do {
            $oldname = People::findFirst("name = '{$this->name}'");
            $oldalias = People::findFirst("alias = '{$this->alias}'");
            if( !empty($oldname) ){
                $this->name = $name.'('.$i.')';
                $pinyin = new Pinyin();
                $this->alias = $pinyin->converter( $this->name );
            }elseif( !empty($oldname) ){
                $this->alias .= '('.$i.')';
            }
            $i++;
        }while( !empty($oldname) && !empty($oldalias) );
    }


    /**
     * 轉換特定欄位
     * rule1. 建立時間/最後登入時間
     * rule2. 狀態碼
     * rule3. 隱藏密碼
     */
    public function afterFetch()
    {
        if( isset($this->lastTime) )
            $this->lastTimeFt = date('Y-m-d H:i:s', $this->lastTime);

        if( isset($this->initTime) )
            $this->initTimeFt = date('Y-m-d H:i:s', $this->initTime);
    }


    /**
     */
    public function afterCreate(){}

}
