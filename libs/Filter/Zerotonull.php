<?php

namespace Personalwork\Filter;

/**
 * 處理空字串轉空值
 */
class Zerotonull extends \Phalcon\Filter
{
    public function filter($value)
    {
        return ($value ==='' or $value === 0 )? null : $value;
    }
}