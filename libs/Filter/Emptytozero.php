<?php

namespace Personalwork\Filter;

/**
 * 處理空字串轉零
 */
class Emptytozero extends \Phalcon\Filter
{
    public function filter($value)
    {
        return (empty($value))? 0 : $value;
    }
}