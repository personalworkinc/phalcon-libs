<?php

namespace Personalwork\Filter;

/**
 * 處理空字串轉空值
 */
class Emptytonull extends \Phalcon\Filter
{
    public function filter($value)
    {
        return (empty($value))? null : $value;
    }
}