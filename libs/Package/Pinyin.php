<?php
/**
 *
 * @package Personalwork
 * @subpackage Plugins
 * @see https://github.com/xuefen/pinyin_array
 */
 
/**
 * 開發可將中文字詞轉換為英文拼音方式套件
 * 此Plugin會自動字串程度，主要由字碼對照資訊進行比對轉換
 * 
 * @author San Haung
 */

namespace Personalwork\Package; 
 
 
class Pinyin
{
    
    /**
     * undocumented class variable
     *
     * @var string
     */
    public $_rootpath;
    
    /**
     * 轉換英文拼音字串
     *
     * @var string
     */
    public $_ping;
    
    
    public function __construct(){
        //設定
        $this->_rootpath = realpath( dirname(__FILE__) );
    }
    
    private function getOrd($word) {
        return ord($word{2})*256*256 + ord($word{1})*256 + ord($word{0});
    }
    
    
    /**
     * Copyright (c) 2012 Xuefen Tong
     * @author Xuefen.Tong
     * @date 2012-2-15
     * 
     * @param string $word 單一中文字
     * 
     * @param bool $isMini 是否使用pings_mini
     * 
     * @return 轉換字碼
     * 
     */
    private function getPing($word, $isMini=false ) {
        //儲存已設定字元對照表
        static $ping_arr = array();
        
        $order = $this->getOrd($word);
        $type = substr($order."", 0, -5);
        
        $path = ( $isMini )? $this->_rootpath.'/Pinyin/pings_mini' : 
                             $this->_rootpath.'/Pinyin/pings' ;
        
        if( !isset($ping_arr[$type]) && is_file("$path/$type.php")) {
           $ping_arr[$type] = include_once("$path/$type.php");
        }
        
        //隱藏當未找到對應字碼時NOTICE訊息
        return @$ping_arr[$type][$order];
    }
    
    
    /**
     * 過濾掉非英文字元，並移除重複"-"與字尾"-"
     */
    private function _remove_non_asc( ){
        $tmp = preg_replace('/[^(\x20-\x7F)]*/','', $this->_ping);
        $tmp = preg_replace('/--/','-', $tmp);
        $in  = array('/-$/', '/^-/');
        $out = array('', ''); 
        $this->_ping = preg_replace($in,$out, $tmp);
    }
    
    
    /**
     * 處理字串轉英拼對照碼
	 * 2013-09-06 修正判斷中文字方式
	 * @see http://stackoverflow.com/questions/4923319/php-check-if-the-string-has-chinese-chars
     * 
     * @param string $words
     * 
     * @return string $this->_ping
     * 
     */
    public function converter($words)
    {
    		/**
		 * 判斷若使用ZF架構(Zend_Validate_Alnum存在)，預先檢查是否為中文字串 
		 */
		if( function_exists('filter_var') ){
			$words = filter_var($words, FILTER_SANITIZE_STRING );
			if( !preg_match("/\p{Han}+/u", $words) ){
		    		//echo $words."<BR/>";
				return $words;
			}
		}
		// echo $words."<BR/>";
		
        while ($words!="") {
            $w		= mb_substr($words, 0, 1, "UTF-8");
            $words	= mb_substr($words, 1, strlen($words)-1, "UTF-8");
            
			if( function_exists('filter_var') ){
				$w = filter_var($w, FILTER_SANITIZE_STRING );
				//直接判斷非中文字方式
				if( !preg_match("/\p{Han}+/u", $w) ){
					$p = $w;
				}else{
	    		        $p = $this->getPing($w);
				}
			}
            $tmp[] =  ( $p =="" ? $w : $p );
        }
        $this->_ping = implode("-", $tmp);
        
        $this->_remove_non_asc();
        
        return $this->_ping;
    }   
}