<?php
/**
 * 處理在Action執行期間判斷@Jslibrary註釋解析附加Assets項目
 * 透過$di['dispatcher']直接註冊
 *
 * Packages list
 * 1. datatable
 * 2. dataTable-yadcf
 * 3. moment
 * 4. datetimepicker
 * 5. jqueryui-datepicker
 * 6. formvalid(jquery-validation、jquery-validation-pkextend)
 * 7. inputuplaod(bootstrap-fileinput)
 * 8. richeditor
 * 9. typeahead
 */
namespace Personalwork\Annotations;

use \Phalcon\Mvc\User\Plugin as UserPlugin;
use \Phalcon\Events\Event;
use \Phalcon\Mvc\Dispatcher;

class LoadAssets extends UserPlugin
{
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $annotations = $this->annotations->getMethod(
            $dispatcher->getControllerClass(),
            $dispatcher->getActiveMethod()
        );

        if( $annotations->has('Jslibrary') ){
            $annotation = $annotations->get("Jslibrary");

            // 直接將@Phtml指定為變數
            $jsHeader = array(); $jsFooter = array(); $css = array();
            foreach ($annotation->getArguments() as $index => $package) {
                // var_dump($package);
                switch($package)
                {
                    // dataTable
                    case 'datatable':
                    $jsHeader[] = '/misc/bower_component/datatables.net/js/jquery.dataTables.min.js';
                    $css[] = '/misc/bower_component/datatables.net-dt/css/jquery.dataTables.min.css';
                    // $jsFooter[] = '/misc/js/pk-datatable.js';
                        break;

                    // dataTable-yadcf
                    case 'dtyadcf':
                    $jsHeader[] = '/misc/bower_component/yadcf/jquery.dataTables.yadcf.js';
                    $css[] = '/misc/bower_component/yadcf/jquery.dataTables.yadcf.css';
                        break;

                    // moment
                    case 'moment':
                    $jsHeader[] = '/misc/bower_component/moment/min/moment.min.js';
                    $jsHeader[] = '/misc/bower_component/moment/locale/zh-tw.js';
                        break;

                    /**
                     * 目前透過dtyadcf套件使用日期選擇器似乎會因為樣式問題無法正確將日期選擇功能掛在文字欄位之下顯示
                     * 故透過dtyadcf時使用jquery-ui
                     * */
                    case 'datetimepicker';
                    case 'dtyadcf_datetimepicker':
                    // 單純載入日期選擇器
                    $jsHeader[] = '/misc/bower_component/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js';
                    $css[] = '/misc/bower_component/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
                        break;

                    case 'jqueryui-datepicker';
                    // 單純載入日期選擇器
                    $jsHeader[] = '/misc/bower_component/jquery-ui/jquery-ui.min.js';
                    $css[] = '/misc/bower_component/jquery-ui/themes/base/jquery-ui.min.css';
                        break;

                    case 'formelms':
                    $jsHeader[] = '/misc/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/twix.min.js';
                    // 日期選擇器
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js';
                    // 日期時間選擇器
                    $jsHeader[] = '/misc/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js';
                    // 時間選擇器
                    $jsHeader[] = '/misc/bower_component/bootstrap-timepicker/js/bootstrap-timepicker.js';
                    // 欄位預設格式處理inputmask
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.date.extensions.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/inputmask.numeric.extensions.js';
                    $jsHeader[] = '/misc/bower_component/jquery.inputmask/dist/inputmask/jquery.inputmask.js';
                    // 預設偵測樣式判斷載入處理
                    $jsFooter[] = '/misc/js/form-elms.js';
                        break;
                    // jquery-validation、jquery-validation-pkextend
                    case 'formvalid':
                    $jsHeader[] = '/misc/bower_component/jquery-validation/dist/jquery.validate.min.js';
                    $jsHeader[] = '/misc/bower_component/jquery-validation/dist/additional-methods.min.js';
                    // 包含判斷頁面表單套用表單驗證
                    $jsFooter[] = '/misc/bower_component/jquery-validation-pkextend/validation-extend.js';

                    $css[] = '/misc/bower_component/jquery-validation-pkextend/validation-extend.css';
                    break;

                    // bootstrap-fileinput
                    case 'inputuplaod':
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/sortable.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/plugins/purify.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/fileinput.min.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/js/locales/zh-TW.js';
                    $jsHeader[] = '/misc/js/plugins/bootstrap-fileinput/themes/fa/theme.js';
                    $css[] = '/misc/js/plugins/bootstrap-fileinput/css/fileinput.min.css';
                        break;

                    case 'richeditor':
                    $jsHeader[] = '/misc/bower_component/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js';
                    $css[] = '/misc/bower_component/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css';
                        break;

                    case 'typeahead':
                    $jsHeader[] = '/misc/bower_component/bootstrap3-typeahead/bootstrap3-typeahead.min.js';
                        break;
                }
            }

            if( count($jsHeader) > 0 ){
            foreach($jsHeader as $js){
                $this->assets->collection("headerScript")->addJs($js, true);
            }
            }

            if( count($css) > 0 ){
            foreach($css as $style){
                $this->assets->collection("headerStyle")->addCss($style, true);
            }
            }

            if( count($jsFooter) > 0 ){
            foreach($jsFooter as $js){
                $this->assets->collection("footerScript")->addJs($js, true);
            }
            }
        }
    }
}
