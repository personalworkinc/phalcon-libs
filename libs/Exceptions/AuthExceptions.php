<?php

/**
 * 提供針對認證機制方面的例外定義
 * @package Exceptions
 * @since 0.8.4
 */

namespace Personalwork\Exceptions;

/**
 * Personalwork\Exceptions\AuthExceptions
 */
class AuthExceptions extends \Personalwork\Exceptions\Exception
{
  protected $pk_authen_code;

  const AUTHEN_UNAUTHORIZED_NOTFOUND = 4011;

  const AUTHEN_UNAUTHORIZED_IDENTIFY_FAIL = 4011;

  const AUTHEN_UNAUTHORIZED_PASSWORD_FAIL = 4013;

  // 200 => ok
  const AUTHEN_IDENTIFY_ACTIVED_NEED_PASSWORD = 2002;

  const AUTHEN_IDENTIFY_FINISH_ACTIVED = 2003;

  /**
   * 預設自定義認證機制錯誤訊息對照碼
   * @var array
   */
  protected $_mapping = [
    4011 => [
      'const' => 'AUTHEN_UNAUTHORIZED_NOTFOUND',
      'msg' => '並未取得所需參數無法繼續執行。',
    ],
    4012 => [
      'const' => 'AUTHEN_UNAUTHORIZED_IDENTIFY_FAIL',
      'msg' => '未找到使用者資訊。'
    ],

    4013 => [
      'const' => 'AUTHEN_UNAUTHORIZED_PASSWORD_FAIL',
      'msg' => '密碼錯誤。'
    ],

    2002 => [
      'const' => 'AUTHEN_IDENTIFY_ACTIVED_NEED_PASSWORD',
      'msg' => '您已經完成開通，請設定登入密碼。'
    ],
    2003 => [
      'const' => 'AUTHEN_IDENTIFY_FINISH_ACTIVED',
      'msg' => '您已經完成開通與註冊，請使用密碼直接登入使用。'
    ]
  ];


  public function __construct($const_code)
  {
    parent::__construct($const_code);
  }
}
