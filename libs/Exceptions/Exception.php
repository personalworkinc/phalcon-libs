<?php

/**
 * 預設基本的例外定義
 * @package Exceptions
 * @since 0.8.4
 */

namespace Personalwork\Exceptions;

/**
 * Personalwork\Exceptions\Exception
 */
class Exception extends \Phalcon\Exception
{
  protected $pk_code;

  // 406 => Not Acceptable
  const APPLICATION_PARAMS_NOTFOUND = 4061;

  const APPLICATION_ROWDATA_NOTFOUND = 4062;

  // 200 => OK
  const APPLICATION_ROWDATA_PROCESSED = 2001;

  // 500 =>  Internal Server Error
  const DATABASE_INSERT_ERROR = 5001;

  const FILE_UPLOAD_ERROR = 5002;

  const DATABASE_PHALCON_MODEL_ERROR = 5003;

  /**
   * 預設自定義錯誤訊息對照碼
   * @var array
   */
  protected $_mapping = [
    4061 => [
      'const' => 'APPLICATION_PARAMS_NOTFOUND',
      'msg' => '並未取得所需參數無法繼續執行。',
    ],
    4062 => [
      'const' => 'APPLICATION_ROWDATA_NOTFOUND',
      'msg' => '未找到對應資料紀錄無法繼續執行。'
    ],

    //
    2001 => [
      'const' => 'APPLICATION_ROWDATA_PROCESSED',
      'msg' => '該程序已經處理過不需執行。'
    ],

    // 使用特定訊息
    5001 => [
      'const' => 'DATABASE_INSERT_ERROR',
      'msg' => null
    ],
    5002 => [
      'const' => 'FILE_UPLOAD_ERROR',
      'msg' => null
    ],
    5003 => [
      'const' => 'DATABASE_PHALCON_MODEL_ERROR',
      'msg' => null
    ],

  ];


  public function __construct($const_code)
  {
    $this->code = intval(substr($const_code, 0, 3));
    $this->pk_code = $const_code;
    $this->message = $this->_mapping[$const_code]['msg'];
  }


  /**
   * 可自訂訊息，例如資料庫錯誤
   *
   * @param string $msg
   * @return void
   */
  public function setMessage($msg)
  {
    $this->message = $msg;
    return $this;
  }

  public function getCustomCode()
  {
    return $this->_mapping[$this->pk_code];
  }

  public function response()
  {
    $resp = [
      'httpstatus' => $this->code,
      'code' => $this->pk_code,
      'codeType' => $this->_mapping[$this->pk_code]['const'],
      'customMsg' => $this->message,
      'errMsg' => $this->_mapping[$this->pk_code]['msg'],
      'occurline' => $this->line
    ];
    return $resp;
  }
}
