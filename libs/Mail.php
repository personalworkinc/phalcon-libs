<?php
namespace Personalwork;

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View,
    Swift_Mailer,
    Swift_Attachment,
    Swift_Message,
    Swift_SmtpTransport;

/**
 * Phalcon\UserPlugin\Mail\Mail
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{
    protected $_transport;

    protected $_directSmtp = true;

    protected $attachments = array();
    
    /**
     * Adds a new file to attach
     *
     * @param unknown $file
     */
    public function addAttachment($name, $content, $type='text/plain')
    {
        $this->attachments[] = array(
            'name' => $name,
            'content' => $content,
            'type' => $type
        );
    }

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array  $params
     */
    public function getTemplate($name, $params)
    {
        // $parameters = array_merge(array(
            // 'publicUrl' => $this->config->application->publicUrl,
        // ), $params);
        
        $this->view->setViewsDir(PPS_APP_APPSPATH . '/backend/views/')
                   ->setLayout('emailtemplate-layout');
        
        return $this->view->getRender('emailTemplate', $name, $params, function ($view) {
                $view->setRenderLevel(View::LEVEL_LAYOUT);
        });

    }

    /**
     * Sends e-mails based on predefined templates. If the $body param
     * has value, the template will be ignored
     *
     * @param array  $to('email address'=>'display name')
     * @param string $subject
     * @param string $name    Template name
     * @param array  $params
     * @param array  $body
     */
    public function send($to, $subject, $name = null, $params = null, $body = null)
    {
        //Settings
        $mailSettings = $this->config->mail;
        

        $template = $body ? $body : $this->getTemplate($name, $params);
        // Create the message
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom(array(
                $mailSettings->fromEmail => $mailSettings->fromName
            ))
            ->setBody($template, 'text/html');

        // Check attachments to add
        foreach ($this->attachments as $file) {
            $message->attach( Swift_Attachment::newInstance()
                    ->setBody($file['content'])
                    ->setFilename($file['name'])
                    ->setContentType($file['type'])
            );
        }

        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
                $mailSettings->smtp->server,
                $mailSettings->smtp->port,
                $mailSettings->smtp->security
            )
            ->setUsername($mailSettings->smtp->username)
            ->setPassword($mailSettings->smtp->password);
        }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($this->_transport);
        $result = $mailer->send($message);

        $this->attachments = array();

        return $result;
    }


    /**
     * Send a raw e-mail via AmazonSES
     * @param string $raw
     */
    private function amazonSESSend($raw)
    {
        if ($this->amazonSes == null) {
            $this->amazonSes = new \AmazonSES(
                $this->config->amazon->AWSAccessKeyId,
                $this->config->amazon->AWSSecretKey
            );
            $this->amazonSes->disable_ssl_verification();
        }
        $response = $this->amazonSes->send_raw_email(array(
            'Data' => base64_encode($raw)
        ), array(
            'curlopts' => array(
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0
            )
        ));
        if (!$response->isOK()) {
            throw new Exception('Error sending email from AWS SES: ' . $response->body->asXML());
        }
        return true;
    }
}
