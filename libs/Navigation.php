<?php
/**
 * 透過Modules config可建立 Nagivation 機制
 *
 * @package Personalwork
 * @subpackage Navigation
 * @author San Haung <san@mail.personalwork.tw>
 * @version 2014122101
 *
 * - 2014-11-24
 * 1. 增加icon變數，讓選單可以前置產生icon圖示
 *
 */

/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 *
 */
namespace Personalwork;

class Navigation {

    /**
     * Navigation config
     *
     * @var \Phalcon\Config
     */
    private $_config = null;

    /**
     * Collection of navigations
     *
     * @var array
     */
    private $_navigations = array();

    /**
     * Build all navigation with confoguration
     *
     * @param type $config
     */
    public function __construct($config) {
        $this->_config = $config;
        foreach ($this->_config as $navigationName => $conf) {
            if( !isset($conf->disableNav) ){
                $conf->name = $navigationName;
                $this->_navigations[$navigationName] = $this->mapNode($conf);
            }
        }
    }

    /**
     * Return navigation from configure structure
     * @return object
     */
    public function getNavConfigure($name) {
        if (!key_exists($name, $this->_config))
            return null;
        return $this->_config->$name;
    }

    /**
     * Return all navigation collection
     *
     * @param type $name
     * @return array
     */
    public function getNavigation($name) {
        if (!key_exists($name, $this->_navigations))
            return null;

        return $this->_navigations[$name];
    }

    /**
     * Generate HTML code
     *
     * @param type $name
     * @return type
     */
    public function toHtml($name, $format='bar') {
        $helper = new \Personalwork\Mvc\View\Helpers\Navigate();
        return $helper->toHtml($this->getNavigation($name), $format);
    }

    /**
     * Collection mapper
     *
     * @param type $config
     * @param type $parent
     * @return array
     */
    public function mapCollection($config, $parent = null) {
        $collection = array();
        foreach ($config as $nodeConfig) {
            $node = $this->mapNode($nodeConfig);
            $node->setParent($parent);
            $collection[] = $node;
        }
        return $collection;
    }

    /**
     * Node mapper
     *
     * @param type $config
     * @return \Modules\Navigation\Node
     */
    public function mapNode($config) {

        $node = new \Personalwork\Navigation\Node();
        $allData = $config->toArray();

        foreach($allData as $key => $value){
            if($key == 'childs')
                continue;

            if( method_exists($node, 'set'.ucfirst($key)) )
                $node->{'set'.ucfirst($key)}($value);
        }

        if (isset($config->childs)) {
            $collection = $this->mapCollection($config->childs, $node);
            $node->setChilds($collection);
        }

        return $node;
    }

    /**
     * Set active node in all navigation collections.
     *
     * @param type $action
     * @param type $controller
     * @param type $module
     * @param type $params
     * @return void
     */
    public function setActiveNode($action, $controller, $module, $params=null) {
        $this->dissactiveNodes();

        foreach ($this->_navigations as $navigation) {
            $this->_activeCollection($navigation->getChilds(), $action, $controller, $module, $params=null);
        }
    }

    /**
     * Activate all active nodes in collection.
     *
     * @param type $collection
     * @param type $action
     * @param type $controller
     * @param type $module
     * @param type $params
     * @return void
     */
    public function _activeCollection($collection, $action, $controller, $module, $params=null) {

        foreach ($collection as $node) {
            if ($node->getAction() == $action &&
                    $node->getController() == $controller &&
                        $node->getModule() == $module) {

                $this->_activateNode($node);
            }

            if ($node->hasChilds())
                $this->_activeCollection($node->getChilds(), $action, $controller, $module);
        }
    }

    /**
     * Activate node
     *
     * @param type $node
     * @return void
     */
    private function _activateNode($node)
    {
        $node->setActive(true);
        if (!is_null($node->getParent()))
                $this->_activateNode ($node->getParent());
    }


    /**
     * Dissactive all nodes in collection
     *
     * @param type $collection
     */
    private function _dissactiveCollection($collection) {
        foreach ($collection as $node) {
            $node->setActive(false);
            if ($node->hasChilds())
                $this->dissactiveCollection($node->getChilds());
        }
    }

    /**
     * Dissactive all nodes in all collections
     */
    public function dissactiveNodes() {
        foreach ($this->_navigations as $navigation) {
            $this->_dissactiveCollection($navigation);
        }
    }

}
