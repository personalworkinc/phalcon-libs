<?php

/******************************************
 *  Personalwork Phalcon System
 * 身份驗證檢查機制
 * 
 * @TODO
 * 1. 基本版(比對config.php內auth->[backend/frontend]設定參數)
 * 2. 進階版(比對資料庫 people table)
 * 3. 例外錯誤訊息放入AuthException 
 * 
 ******************************************/

 namespace Personalwork\Auth;
 
 use Phalcon\Events\Event,
Phalcon\Mvc\Dispatcher,
Phalcon\Mvc\User\Plugin;

use Personalwork\Exceptions\AuthExceptions as Exception;

/**
 * Personalwork\Plugins\Auth
 */
 class AuthPlugin extends Plugin {
     
    /**
     * 基本版驗證
     */
    const TYPE_BASIC = 1;
    
    /**
     * 進階版驗證
     */
    const TYPE_ADVANCE = 2;
    
    var $type;
     
     
    /**
     * Allowed resource types for the configuration file
     * @var array
     */
    private $st_resourceTypes = array(
        'public',
        'private'
    );
     
     
    public function __construct($opt=null) {
         
        if( is_array($opt) ){
            foreach ($opt as $key => $value) {
                $this->$key = $value;
            }
        }
         
    }
     
    /**
     * beforeDispatchLoop
     *
     * @param  Event                           $event
     * @param  Dispatcher                      $dispatcher
     * @return \Phalcon\Http\ResponseInterface
    */
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        if ($this->auth->hasRememberMe()) {
            $this->auth->loginWithRememberMe(false);
        }
        
        $config = $dispatcher->getDI()->get('config');
        $authConfig = $this->getConfigStructure($config);
        $needsIdentity = $this->needsIdentity($authConfig, $dispatcher);
        $identity = $this->auth->getIdentity();
        
        if ($this->auth->isUserSignedIn()) {

            $actionName     = $dispatcher->getActionName();
            $controllerName = $dispatcher->getControllerName();
            
            if ($controllerName == 'people' && $actionName == 'login') {
                // 判斷前後端驗證導向  
                if( in_array($user->RoleId ,array('1','2')) ){
                    $authRedirect = $config->auth->backend->success;
                }elseif( in_array($user->RoleId ,array('3')) ){
                    $authRedirect = $config->auth->frontend->success;
                }
                
                return $this->response->redirect($authRedirect);
            }
        }
      
        if (true === $needsIdentity) {
            if (!is_object($identity)) {
                $this->flash->notice('Private area. Please login.');

                $this->view->disable();
                return $this->response->redirect($config->auth->frontend->failure)->send();
            }
        }

        $this->view->setVar('identity', $identity);
    }

    /**
     * Check if the controller / action needs identity
     *
     * @param  array      $config
     * @param  Dispatcher $dispatcher
     * @return boolean
     */
    protected function needsIdentity($config, Dispatcher $dispatcher)
    {
        $actionName     = $dispatcher->getActionName();
        $controllerName = $dispatcher->getControllerName();

        if ($config['type'] == 'public') { // all except ..

            return $this->checkPublicResources($config['resources'], $actionName, $controllerName);
        } else {
            return $this->checkPrivateResources($config['resources'], $actionName, $controllerName);
        }

        return false;
    }

    /**
     * Check for public resources
     *
     * @param  array   $resources
     * @param  string  $actionName
     * @param  string  $controllerName
     * @return boolean
     */
    protected function checkPublicResources($resources, $actionName, $controllerName)
    {
        $resources = isset($resources['*']) ? $resources['*'] : $resources;

        foreach ($resources as $controller => $actions) {
            if ($controller == $controllerName) {
                if (isset($controller['*'])) {
                    return true;
                } else {
                    if (in_array($actionName, $actions) || $actions[0] == '*') {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check for private resources
     *
     * @param  array   $resources
     * @param  string  $actionName
     * @param  string  $controllerName
     * @return boolean
     */
    protected function checkPrivateResources($resources, $actionName, $controllerName)
    {
        $resources = isset($resources['*']) ? $resources['*'] : $resources;

        foreach ($resources as $controller => $actions) {
            if ($controller == $controllerName) {
                if (isset($controller['*'])) {
                    return true;
                } else {
                    if (in_array($actionName, $actions)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get the configuration structure for the plugin
     *
     * @param  \Phalcon\Config $config
     * @throws Exception
     */
    protected function getConfigStructure(\Phalcon\Config $config)
    {
        if (isset($config->auth)) {
            $config = $config->auth->resources->toArray();

            if (!isset($config['type']) || (isset($config['type']) && !in_array($config['type'], $this->st_resourceTypes))) {
                throw new Exception('Wrong configuration for key "type" or the key does not exists');
            }

            if (!isset($config['resources']) || (isset($config['resources']) && !is_array($config['resources']))) {
                throw new Exception('Resources key must be an array');
            }

            return $config;
        } else {
            throw new Exception('Configuration error: I couldn\'t find the configuration key "auth" ');
        }
    }
}
 

?>
