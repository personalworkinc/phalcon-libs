<?php

namespace Personalwork\Forms;

class FormGroups extends \Personalwork\Forms\Form
{
    /**
     * 建立群組欄位
     * 
     * @var array
     */
    protected $elementsGroup = array();
    
    public function getFieldSet() {
        return $this->elementsGroup;
    }
    
    /**
     * Set custom tag
     * 
     * @param type $customTags
     * @return \Modules\Form
     */
    public function addFieldSet($confset)
    {
        // 將所有元素物件加到表單物件
        $elements = $this->_getElementInFeilds($confset);
        $confset->elements = $elements;
        //配置紀錄群組元素結構
        $this->elementsGroup[] = $confset;
        
        return $this;
    }
    
    
    /**
     * 根據傳入定義在json內表單元素的鍵值[key]來取得特定表單元素
     * 若未找到回傳FALSE
     * 主要由Form View Helper呼叫以建立群組內符合元素樣式
     * 
     * @param string|array $key
     * @return \Phalcon\Froms\Eelement
     */
    public function findElementByKey( $key ){
        if( empty($key) ) return FALSE;
        
        $handle = ( is_string($key) )? array($key=>true) : $key;
        
        $elms = array_intersect_key($this->getElements(), $handle);
        if( count($elms) == 0 ) {
            return FALSE;
        }else{
            return $elms;
        }
    }
    
    
    private function _getElementInFeilds($fieldSet) {
        
            foreach( $fieldSet->elements as $i => $elm){
                $class = '\\Phalcon\\Forms\\Element\\'.ucfirst($elm->type);
            if( $elm->type == 'select' ){
                $element = new $class( $elm->key ,$elm->options->toArray());
            }else{
                $element = new $class( $elm->key );
            }
            $element->setLabel($elm->label);
            
            if( isset($this->config->sysconfig) ){
                $d = $this->config->sysconfig->toArray();
                $element->setDefault($d[$elm->key]);
            }else{
                $element->setDefault($elm->default);
            }
            
            if( isset($elm->options) ){
                $element->setUserOptions($elm->options->toArray());
            }
            if( isset($elm->attributes) ){
                $element->setAttributes( $elm->attributes->toArray() );
            }
            
            //自行配置新的Attribute for element decorator
            $element->decorator = $elm->decorator; 
            
            if( isset($elm->validators) ){
                foreach($elm->validators as $validator => $option){
                    $classname = '\\Phalcon\\Validation\\Validator\\'.$validator;
                    if( is_object($option) )
                        $element->addValidator(new $classname($option->toArray()));
                    else
                        $element->addValidator(new $classname($option));
                }
            }
            $this->add($element);
            
            //Array( element_key => ture )
            $elements[$elm->key] = true;
        }
            
        return $elements;
    }

    /**
     * Render all Form
     * ( prepare for overrider in seperate project )
     */
    public function toHtml(){}
    
}
