<?php

namespace Personalwork\Forms;

use Phalcon\Tag;

/**
 * 配置hidden表單元素，附加可直接配置Decorator作法
 */
class Elements extends \Phalcon\Forms\Element
{
    /**
     * 產出的html結構
     * */
    var $html = '';

    /**
     * html結構裝飾器
     * */
    var $decorator;

    function __construct($name, $default=null, $opt=array())
    {
        $this->setName($name);

        if( !is_null($default) ){
            $this->setDefault($default);
        }

        if( count($opt) > 0 ){
            foreach ($opt as $key => $value) {
                // 若方法存在直接配置
                if( method_exists($this, 'set'.ucfirst($key) ) ){
                    $this->{'set'.ucfirst($key)}($value);
                // addFilter / addFilters / addValidator / addValidators
                }elseif( method_exists($this, 'add'.ucfirst($key) ) ){
                    $this->{'add'.ucfirst($key)}($value);
                // 配置裝飾者
                }elseif( $key == 'decorator' ){
                    if( class_exists( $value ) ){
                        $this->decorator = new $value();
                        $this->decorator->setElement($this);
                    }
                }
            }
        }
    }

    public function getDecorator(){
        return $this->decorator;
    }

    /**
     * @param string $decorator DecoratorCalssName
     * */
    public function setDecorator($decorator){
        $this->decorator = new $decorator();
        $this->decorator->setElement($this);
    }

    public function render($attributes = [])
    {
        $attr['id'] = $this->getName();
        $attr['name'] = $this->getName();
        $attr['value'] = $this->getValue();

        $attr = @array_merge($attr, $this->getAttributes(), $attributes);

        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= "\t".Tag::tagHtml('div', ['class'=>'form-group'], FALSE, TRUE, TRUE).PHP_EOL;
        }

        // label
        if( !empty($this->getUserOption('label-class')) ){
            $this->html .= "\t\t\t".Tag::tagHtml( 'label',
                                                    array(
                                                    'for'   => $this->getName(),
                                                    'class' => 'col-sm-2 '.$this->getUserOption('label-class')
                                                    ), FALSE, TRUE, TRUE);
            $this->html .= $this->getLabel();
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }
        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= "\t".Tag::tagHtml('div', ['class'=>'col-sm-10'], FALSE, TRUE, TRUE).PHP_EOL;
        }
            $this->html .= "\t\t".Tag::tagHtml('input', $attr, FALSE, TRUE, TRUE).PHP_EOL;
            if( !empty($this->getUserOption('postfix-label')) ){
                $this->html .= "\t\t\t".Tag::tagHtml( 'label',
                                                        array(
                                                        'class' => $this->getUserOption('label-class')
                                                        ), FALSE, TRUE, TRUE);
                $this->html .= $this->getUserOption('postfix-label');
                $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
            }
        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= Tag::tagHtmlClose('div').PHP_EOL;
        }

        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }
}
