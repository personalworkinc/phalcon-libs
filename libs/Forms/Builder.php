<?php
/**
 * Builder - 根據Model產生表單類別功能
 *
 * 1. 從table讀取欄位附加備註資訊
 * 2. 改為\Personalwork\Forms\Builder類別呼叫
 * 3. 從任何router下透過產生物件方式執行, usage:
 *
 * */

namespace Personalwork\Forms;

const BR = '<BR/>';

/**
 * usage :
 *
 * $fb= new \Personalwork\Forms\Builder([
                'DI'        => $this->getDI(),
                'path'      => 'houserich/forms/',
                'inclusive' => ['ModelName']
             ]);

        $msg = $fb->setNamespace("House\\Houserich\\Forms")
                  ->run()
                  ->getMessages();
        echo implode('<br/>', $msg);
 */
class Builder
{
    const NL = "\n";
    const TAB = "\t";

    /**
     * Dependency Injector
     * */
    var $DI;


    /**
     * 表單類別儲存路徑
     * */
    var $path;


    /**
     * 過程紀錄訊息
     * */
    var $messages;

    /**
     * @param mixed 指定清單，以modelname設定
     * ex: tablename case => "Case"
     * */
    protected $inclusive = array();

    /**
     * @param mixed 排除清單，以modelname設定
     * ex: tablename case => "Case"
     * */
    protected $exclusive = array();

    /**
     * @param mixed 實際進行產生Form的資料表清單
     * */
    private $listtables;

    /**
     * @param string 表單名稱空間
     * */
    private $namespace;

    /**
     * @param string 繼承表單類別
     * */
    private $extends = "\\Personalwork\\Forms\\Form";


    /**
     * 預設必須至少配置path
     * Supported attributes:
     * - object DI
     * - string path        以 PPS_APP_APPSPATH 為根目錄的表單儲存路徑
     * - Array  inclusive   指定處理的資料表[tablename]
     * */
    public function __construct($opt=array()) {

        if( empty($opt['DI']) ){
            die('必須傳入$opt[\'DI\']物件!');
        }

        $this->DI = $opt['DI'];
        $config = $this->DI->get('config');
        $this->db = $opt['DI']->get('db');

        //預設命名空間
        $this->namespace = ucfirst(PPS_APP_PROJECTNAME)."\\Forms";

        if( !empty($config->application->formsDir) &&
            is_dir($config->application->formsDir) ){
            $this->path = realpath($config->application->formsDir);
        }

        if( isset($opt['path']) ){
            if( is_dir(PPS_APP_APPSPATH.DIRECTORY_SEPARATOR.$opt['path']) ) {
                $this->path=realpath(PPS_APP_APPSPATH.DIRECTORY_SEPARATOR.$opt['path']);
            }else{
                die('設定路徑:'.PPS_APP_APPSPATH.DIRECTORY_SEPARATOR.$opt['path'].' 不存在或無存取權限！');
            }
        }

        if( !realpath($this->path) ){
            die('表單儲存路徑:'.$this->path.' 讀取錯誤，請修正!');
        }

        if( !empty($opt['inclusive']) && is_array($opt['inclusive']) ){
            $this->setInclusive($opt['inclusive']);
        }else{
            $this->_prepare_listtables();
        }
    }


    public function getPath() {
        return $this->path;
    }

    /**
     * 設定產生表單命名空間
     */
    public function setNamespace($namespace) {
        $this->namespace = $namespace;
        return $this;
    }


    public function getMessages() {
        return $this->messages;
    }

    /**
     * @param Array 產生表單類別清單
     * */
    public function setInclusive($table){
        $this->inclusive = $table;
        $this->_prepare_listtables();
        return $this;
    }

    public function getListTables() {
        return $this->listtables;
    }


    /**
     * 產生需建立對應表單類別清單
     * */
    private function _prepare_listtables() {
        if( !empty($this->inclusive) ){
            $this->listtables = $this->inclusive;
        }else{
            $tables = $this->db->listTables();
            foreach ($tables as $table) {
                $tablename = \Phalcon\Text::camelize($table);
                if( !empty($this->exclusive) && in_array($tablename, $this->exclusive) ){
                    continue;
                }else{
                    $this->listtables[] = $tablename;
                }
            }
        }
    }


    /**
     * Return a better Phalcon type (Check, Select, Password, ...) according to previous Phalcon type
     * and size of the column
     * @param string $type
     * @param \Phalcon\Db\Column $column
     * @return string
     */
    private function _getType($type, $column)
    {
        $size = $column->getSize();
        $columntype = $type;
        if ($type == "Numeric") {
            if ($size == 1) {
                $columntype = "Check";
            } else if ($size == 11) {
                $columntype = "Select";
            }
        } else if ($type == "Text") {
            if ($size > 255) {
                $columntype = "Textarea";
            }
            if (preg_match("#(pass|pwd)#i", $column->getName())) {
                $columntype = "Password";
            }
        }
        return $columntype;
    }


    /**
     * Return the Phalcon type (Numeric, Date, Text, ...) according to db type
     * @param string $type
     * @return string
     */
    private function _getBaseType($type)
    {
        $types = [
            \Phalcon\Db\Column::TYPE_INTEGER    => "Numeric",
            \Phalcon\Db\Column::TYPE_DATE       => "Date",
            \Phalcon\Db\Column::TYPE_VARCHAR    => "Text",
            \Phalcon\Db\Column::TYPE_DECIMAL    => "Numeric",
            \Phalcon\Db\Column::TYPE_DATETIME   => "Text",
            \Phalcon\Db\Column::TYPE_CHAR       => "Text",
            \Phalcon\Db\Column::TYPE_TEXT       => "Textarea",
            \Phalcon\Db\Column::TYPE_FLOAT      => "Numeric",
            \Phalcon\Db\Column::TYPE_BOOLEAN    => "Numeric",
            \Phalcon\Db\Column::TYPE_DOUBLE     => "Numeric",
            \Phalcon\Db\Column::TYPE_TINYBLOB   => "Textarea",
            \Phalcon\Db\Column::TYPE_BLOB       => "Textarea",
            \Phalcon\Db\Column::TYPE_MEDIUMBLOB => "Textarea",
            \Phalcon\Db\Column::TYPE_LONGBLOB   => "Textarea",
            \Phalcon\Db\Column::TYPE_BIGINTEGER => "Numeric",
            \Phalcon\Db\Column::TYPE_JSON       => "Textarea",
            \Phalcon\Db\Column::TYPE_JSONB      => "Textarea",
            //\Phalcon\Db\Column::TYPE_TIMESTAMP  => "",
            17                                  => "Numeric",
        ];
        return isset($types[$type]) ? $types[$type] : "Text";
    }


    /**
     * @View(pageHeader="根據資料庫table建立表單",render="")
     * */
    public function run()
    {
        foreach ($this->listtables as $modeltablename) {

            $fd = fopen("{$this->path}/{$modeltablename}Form.php", "w");
            $table = strtolower($modeltablename);
            // Begin class
            fwrite($fd, "<?php" . self::NL . self::NL);

            // namespace
            $spacestr = "namespace {$this->namespace};". self::NL. self::NL;
            fwrite($fd, $spacestr );

            // static set Phalcon's Classes
            $phalcons = array();
            $phalcons[] = "use Phalcon\\Tag;". self::NL;
            $phalcons[] = "use Phalcon\\Validation\\Validator\\PresenceOf;". self::NL;
            $phalcons[] = "use Phalcon\\Validation\\Validator\\Numericality;". self::NL;
            $phalcons[] = "use Phalcon\\Validation\\Validator\\Regex;". self::NL;
            $phalcons[] = "use Phalcon\\Validation\\Validator\\StringLength;". self::NL.self::NL.self::NL;
            fwrite($fd, implode('', $phalcons) );

            // extends
            if( !empty($this->extends) ){
                $classstr = "class {$modeltablename}Form extends {$this->extends}". self::NL;
                $classstr.= "{". self::NL;
            }else{
                $classstr = "class {$modeltablename}Form ". self::NL;
                $classstr.= "{". self::NL;
            }
            fwrite($fd, $classstr );

            // Column
            $columns = $this->db->describeColumns($table);
            // 取得欄位備註資訊
            $cols = $this->db->fetchAll("SHOW FULL COLUMNS FROM `{$table}`");
            foreach ($columns as $column) {
                if ($column instanceof \Phalcon\Db\Column) {
                    // Escape if column is primary
                    if ($column->isPrimary())
                        continue;
                    // get Index from assign field
                    $key=array_search($column->getName(), array_column($cols, 'Field'));
                    // get field comment
                    $comment=$cols[$key]['Comment'];

                    // Begin method
                    $columnname = \Phalcon\Text::camelize($column->getName());
                    fwrite($fd, self::NL);
                    // write comment block
                    $commentBlock = <<<BLOCK
/**
\t * @Comment("{$comment}")
\t */
BLOCK;
                    fwrite($fd, self::TAB . $commentBlock . self::NL);

                    fwrite($fd, self::TAB . "private function _{$columnname}() {" . self::NL);
                    // Write element
                    $columntype_base = $this->_getBaseType($column->getType());
                    $columntype = $this->_getType($columntype_base, $column);

                    // 預設配置 Personalwork\Forms\Elements 若該class不存在才用Phalcon\Forms
                    if( class_exists("\\Personalwork\\Forms\\Elements\\{$columntype}") ){
                        fwrite($fd, self::TAB . self::TAB . "\$element = new \\Personalwork\\Forms\\Elements\\{$columntype}(\"{$column->getName()}\");" . self::NL);
                    }else{
                        fwrite($fd, self::TAB . self::TAB . "\$element = new \\Phalcon\\Forms\\Element\\{$columntype}(\"{$column->getName()}\");" . self::NL);
                    }

                    fwrite($fd, self::TAB . self::TAB . "\$element->setLabel(\"{$comment}\");" . self::NL);
                    // Add empty selection for select fields
                    if ($columntype == "Select") {
                        fwrite($fd, self::TAB . self::TAB . "\$element->setOptions([]);" . self::NL);
                    }
                    // Add validator on text fields
                    if ($columntype == "Text" && $column->getSize() > 0) {
                        fwrite($fd, self::TAB . self::TAB . "\$element->addValidator(new StringLength([" . self::NL);
                        fwrite($fd, self::TAB . self::TAB . self::TAB . "\"max\" => {$column->getSize()}" . self::NL);
                        fwrite($fd, self::TAB . self::TAB . "]));" . self::NL);
                    }
                    // End method
                    fwrite($fd, self::TAB . self::TAB . "return \$element;" . self::NL);
                    fwrite($fd, self::TAB . "}" . self::NL);
                }
            }

            fwrite($fd, self::NL);

            // Final method : initialize(), construction of the form
            fwrite($fd, self::TAB . "public function initialize() {" . self::NL);
            foreach ($columns as $column) {
                if ($column instanceof \Phalcon\Db\Column) {
                    if ($column->isPrimary())
                        continue;
                    $columnname = \Phalcon\Text::camelize($column->getName());
                    fwrite($fd, self::TAB . self::TAB . "\$this->add(\$this->_{$columnname}());" . self::NL);
                }
            }
            fwrite($fd, self::TAB . "}" . self::NL);

            // End class
            fwrite($fd, "}" . self::NL);
            fclose($fd);
        }

        // $this->view->disable();
        $this->messages[] = "產生完成!";
        return $this;
    }
}