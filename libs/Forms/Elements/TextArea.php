<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * 配置textarea表單元素，附加可直接配置Decorator作法
 */
class TextArea extends \Personalwork\Forms\Elements
{
    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function render($attr = [])
    {
        $attr['id'] = $this->getName();
        $attr['name'] = $this->getName();

        foreach ($this->getAttributes() as $key => $value) {
            $attr[$key] = $value;
        }

        if( $this->getValue() ){
            $this->html.= "\t\t\t".Tag::tagHtml('textarea',$attr, FALSE, TRUE, TRUE);
            $this->html.= $this->getValue();
            $this->html.= Tag::tagHtmlClose('textarea').PHP_EOL;
        }else{
            $this->html.= "\t\t\t".Tag::tagHtml('textarea',$attr, TRUE, FALSE, TRUE);
        }


        return $this->html;
    }
}
?>