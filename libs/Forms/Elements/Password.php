<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * 配置password表單元素
 */
class Password extends \Personalwork\Forms\Elements
{
    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function render($attr=['type'=>'password'] ) {
        return parent::render($attr);
    }
}
?>