<?php

namespace Personalwork\Forms\Elements;

/**
 * 配置date表單元素
 */
class Date extends \Personalwork\Forms\Elements
{
    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function render($attr=['type'=>'date'] ) {
        return parent::render($attr);
    }
}
