<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * 配置select表單元素，附加可直接配置Decorator作法
 */
class Select extends \Phalcon\Forms\Element\Select
{
    var $html = '';

    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function render($attr = [])
    {
        $attr['id'] = $this->getName();
        $attr['name'] = $this->getName();

        $attr = array_merge($attr, $this->getAttributes());

        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= "\t".Tag::tagHtml('div', ['class'=>'form-group'], FALSE, TRUE, TRUE).PHP_EOL;
        }

            // label
            if( !empty($this->getUserOption('label-class')) ){
                if( $this->getUserOption('format') == 'form-horizontal' ){
                    $this->setUserOption('label-class', 'col-sm-2 '.$this->getUserOption('label-class') );
                }
                $this->html .= "\t\t\t".Tag::tagHtml( 'label',
                                                        array(
                                                        'for'   => $this->getName(),
                                                        'class' => $this->getUserOption('label-class')
                                                        ), FALSE, TRUE, TRUE);
                $this->html .= $this->getLabel();
                $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
            }

            if( $this->getUserOption('format') == 'form-horizontal' ){
            $this->html .= "\t".Tag::tagHtml('div', ['class'=>'col-sm-10'], FALSE, TRUE, TRUE).PHP_EOL;
            }
                $this->html .= "\t\t".Tag::tagHtml('select', $attr, FALSE, TRUE, TRUE).PHP_EOL;

                foreach ($this->getOptions() as $value => $label) {

                    if( is_string($label) ){
                        // selected
                        $selected = ($value==$this->getValue()) ? 'selected' : null;

                        $this->html .= "\t\t\t".Tag::tagHtml('option', ['value'=>$value, 'selected'=>$selected],FALSE, TRUE, FALSE);
                        $this->html .= str_replace(' ','',$label);
                    }elseif( is_array($label) ){
                        $txt = $label['label'];
                        unset($label['label']);
                        // selected
                        $label['selected'] = ($label['value']==$this->getValue()) ? 'selected' : null;

                        $this->html .= "\t\t\t".Tag::tagHtml('option', $label,FALSE, TRUE, FALSE);
                        $this->html .= str_replace(' ','',$txt);
                    }
                        $this->html .= Tag::tagHtmlClose('option').PHP_EOL;
                }
                $this->html .= "\t\t".Tag::tagHtmlClose('select').PHP_EOL;

            if( $this->getUserOption('format') == 'form-horizontal' ){
            $this->html .= Tag::tagHtmlClose('div').PHP_EOL;
            }

        if( $this->getUserOption('format') == 'form-horizontal' ){
        $this->html .= Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }
}
