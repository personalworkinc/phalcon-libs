<?php

namespace Personalwork\Forms\Elements;

/**
 * 配置number表單元素
 */
class Numeric extends \Personalwork\Forms\Elements
{
    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function render($attr=['type'=>'number'] ) {
        return parent::render($attr);
    }
}
