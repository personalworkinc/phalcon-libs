<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * @see https://github.com/daleffe/phalcon-forms-checkgroup/blob/master/src/Daleffe/Phalcon/Forms/Element/CheckGroup.php
 */
class CheckGroup extends \Personalwork\Forms\Elements
{
    private $checked = null;

    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }


    /**
     * 產出以Housenrich專案前端搜尋配置結構
     * <div class="col-2 txt-ora">縣市</div>
     * <div class="col-10 check-style2">
     *     <label for="option1"><input type="checkbox" id="option1" name="county"><span>台北市</span></label>
     *     <label for="option2"><input type="checkbox" id="option2" name="county"><span>新北市</span></label>
     * </div>
     * */
    public function renderHousenrich() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        if( isset($userOpts['label-class']) ){
        $this->html .= "\t".Tag::tagHtml( 'div', ['class'=>$userOpts['label-class']], FALSE, TRUE, TRUE);
        $this->html .= "\t\t".$this->getLabel();
        $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }

        if( isset($userOpts['parent-class']) ){
            $parent_attr['class'] = $userOpts['parent-class'];
            $parent_attr['id'] = ( isset($userOpts['parent-id']) ) ? $userOpts['parent-id']: null;
            $this->html .= "\t".Tag::tagHtml( 'div', $parent_attr, FALSE, TRUE, TRUE);
        }
        $attr = array();
        foreach ($userOpts['items'] as $i => $item) {
            // 調整checkbox屬性
            $attr['type'] = 'checkbox';
            $attr['id'] = $item['id'];
            $attr['name'] = $this->getName();
            $attr['value'] = $item['value'];
            if( $item['value'] == $this->getValue() ){
                $attr['checked'] = 'checked';
            }elseif( isset($userOpts['default']) && $item['value'] == $userOpts['default'] ){
                $attr['checked'] = 'checked';
            }else{
                $attr['checked'] = null;
            }
            // input:checkbox
            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $item['id'],
                                                    'class'=> $item['class'],
                                                ), FALSE, TRUE, TRUE);
            $this->html .= "\t\t".Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
            $this->html .= "<span>{$item['label']}</span>";
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }

        if( isset($userOpts['parent-class']) ){
        $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }

    /**
     * More decoration than the regular render, does similar things, probably incomplete.
     * Supported attributes:
     * string   `label`             The entire group's label
     * array    `elements`          Key, value associated array
     * string   `format`            Render style (bootstrap3 btn-group:bt3-default)
     * string   `class`             The CSS selectors for each HTML LABEL element
     *
     * @param array $attr Render time attributes, will override what was set earlier.
     * @return string A set of HTML radio elements.
     */
    public function render($attr = [])
    {
        $checked = null; $html = '';

        if (is_array($attr)) {
            foreach ($attr as $key => $value) {
                $this->setAttribute($key, $value);
            }
        } else {
            $attr = $this->getAttributes();
        }

         // Optional attribute to break line each 'n' checkboxes.
        $count = ((isset($attr['linebreakeach']) and is_int($attr['linebreakeach'])) ? 0 : null);

        foreach ($attr['elements'] as $key => $value) {
            if (is_array($this->getValue())) {
                $checked = (array_search($key, $this->getValue()) === false) ? null : ' checked';
            } else {
                $checked = ($key == $this->getValue() && !is_null($this->getValue())) ? ' checked' : null;
            }
            if (!is_null($count) and $count != 0 and $count % $attr['linebreakeach'] === 0) {
                $html .= '<br />';
            }
            $html .= '<input type="checkbox" id="' . $this->getName() . $key . '" name="' . $this->getName() . '[]' . '" value="' . $key . '"' . $checked . ' /> ' . $value . ' ';
            if (!is_null($count)) {
                $count++;
            }
        }

        return $html;
    }
}
