<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * @see https://github.com/daleffe/phalcon-forms-radiogroup/blob/master/src/Daleffe/Phalcon/Forms/Element/RadioGroup.php
 */
class RadioGroup extends \Personalwork\Forms\Elements
{
    var $html = '';

    private $checked = null;

    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    /**
     * 產出以Bootstrap3配置結構
     * <div class="btn-group" data-toggle="buttons">
     *      <label for="" class="radio radio-inline">
     *      <input id="" name="" value="" class="" type="radio" />
     *      </label>
     *      ...
     *      ...
     * </div>
     * */
    public function renderBootstrap3() {
        $attributes = $this->getAttributes();

        $this->html = '<div class="btn-group" data-toggle="buttons">';

        foreach ($attributes['items'] as $label => $value) {
            $checked = ($value == $this->getValue()) ? ' checked' : null;

            $this->html .= '<label for="'.$this->getName().$label.'" class="'.$this->getUserOption('label-class').'">';

            $this->html .= '<input type="radio" id="'.$this->getName().$key.'" name="'.$this->getName().'" value="' . $value . '"' . $checked . ' /> '. $label;

            $this->html .= "</label>";
        }

        if( $format == 'bt3-default' ){
            $this->html .= '</div>';
        }

        return $this->html;
    }


    /**
     * 產出以Kingadmin v1.5配置結構
     * <label for="" class="radio radio-inline">
     *      <input id="" name="" value="" class="" type="radio" />
     * </label>
     * */
    public function renderKingadmin() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        $attr = array();
        foreach ($attributes['items'] as $label => $value) {
            $checked = ($value == $this->getValue()) ? ' checked' : null;

            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $this->getName().$label,
                                                    'class' => $userOpts['label-class']
                                                ), FALSE, TRUE, TRUE);
            // 調整radio屬性
            $attr['type'] = 'radio';
            $attr['id'] = $this->getName().$label;
            $attr['name'] = $this->getName();
            $attr['value'] = $value;
            $attr['checked'] = $checked;

            // input:radio
            $this->html .= "\t\t\t".Tag::tagHtml( 'input', $attr, FALSE, TRUE, TRUE). $label;

            $this->html .= "\t\t".Tag::tagHtmlClose('label').PHP_EOL;
        }

        return $this->html;
    }


    /**
     * 產出以Housenrich專案前端配置結構
     * <div class="radio-style">
     *  <input type="radio" id="male" name="gender" value="男" checked />
     *  <label for="male">男</label>
     *  <input type="radio" id="female" name="gender" value="女" />
     *  <label for="female">女</label>
     * </div>
     * */
    public function renderHousenrich() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        if( isset($userOpts['parent-class']) ){
        $this->html .= "\t".Tag::tagHtml( 'div', ['class'=>$userOpts['parent-class']], FALSE, TRUE, TRUE);
        }

        $attr = array();
        foreach ($userOpts['items'] as $i => $item) {
            // 調整radio屬性
            $attr['type'] = 'radio';
            $attr['id'] = $item['id'];
            $attr['name'] = $this->getName();
            $attr['value'] = $item['value'];
            if( $item['value'] == $this->getValue() ){
                $attr['checked'] = 'checked';
            }elseif( isset($userOpts['default']) && $item['value'] == $userOpts['default'] ){
                $attr['checked'] = 'checked';
            }else{
                $attr['checked'] = null;
            }

            // input:radio
            $this->html .= "\t\t".Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $item['id'],
                                                    'class' => $userOpts['label-class']
                                                ), FALSE, TRUE, TRUE).$item['label'];
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }

        if( isset($userOpts['parent-class']) ){
        $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }


    /**
     * 產出以Housenrich專案前端配置結構
     * <div class="radio-style">
     *  <input type="radio" id="male" name="gender" value="男" checked />
     *  <label for="male">男</label>
     *  <input type="radio" id="female" name="gender" value="女" />
     *  <label for="female">女</label>
     * </div>
     * */
    public function renderHousenrich2() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        if( isset($userOpts['parent-class']) ){
        $this->html .= "\t".Tag::tagHtml( 'div', ['class'=>$userOpts['parent-class']], FALSE, TRUE, TRUE);
        }

        $attr = array();
        foreach ($userOpts['items'] as $i => $item) {
            // 調整radio屬性
            $attr['type'] = 'radio';
            $attr['id'] = $item['id'];
            $attr['name'] = $this->getName();
            $attr['value'] = $item['value'];
            $attr['checked'] = ($item['value'] == $this->getValue()) ? ' checked' : null;

            // input:radio
            $this->html .= "\t\t".Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $item['id'],
                                                    'class' => $userOpts['label-class']
                                                ), FALSE, TRUE, TRUE).$item['label'];
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }

        if( isset($userOpts['parent-class']) ){
        $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }

    /**
     * 產出以Housenrich專案前端搜尋配置結構
     * <span class="radio-style2">
     *   <label for="struct1"><input type="radio" id="struct1" name="struct"><span>開放式</span></label>
     *   <label for="struct1"><input type="radio" id="struct1" name="struct"><span>開放式</span></label>
     * </span>
     * */
    public function renderHousenrich3() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        $this->html .= "\t".Tag::tagHtml( 'span', ['class'=>'radio-style2'], FALSE, TRUE, TRUE);

        $attr = array();
        foreach ($userOpts['items'] as $i => $item) {
            // 調整radio屬性
            $attr['type'] = 'radio';
            $attr['id'] = $item['id'];
            $attr['name'] = $this->getName();
            $attr['value'] = $item['value'];
            if( $item['value'] == $this->getValue() ){
                $attr['checked'] = 'checked';
            }elseif( isset($userOpts['default']) && $item['value'] == $userOpts['default'] ){
                $attr['checked'] = 'checked';
            }else{
                $attr['checked'] = null;
            }
            // input:checkbox
            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $item['id'],
                                                ), FALSE, TRUE, TRUE);
            $this->html .= "\t\t".Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
            $this->html .= "<span>{$item['label']}</span>";
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }

        $this->html .= "\t".Tag::tagHtmlClose('span').PHP_EOL;

        return $this->html;
    }

    /**
     * More decoration than the regular render, does similar things, probably incomplete.
     * Supported attributes:
     * array    `items`          Key, value associated array
     * string   `class`          The CSS selectors for each HTML LABEL element
     * ( use setUserOption() )
     * string   `format`         Render style (bootstrap3、kingadmin)
     *
     * @param array $attr Render time attributes, will override what was set earlier.
     * @return string A set of HTML radio elements.
     */
    public function render($attr = [])
    {
        if (is_array($attr)) {
            foreach ($attr as $key => $value) {
                $this->setAttribute($key, $value);
            }
        }

        // 預設結構樣式（Bootstrap3版本）
        $method  = 'render';
        $method .= ( $this->getUserOption('format') ) ? ucfirst($this->getUserOption('format')) : 'Bootstrap3';
        return $this->{$method}();
    }
}
