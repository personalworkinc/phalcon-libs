<?php

namespace Personalwork\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * 配置button表單元素
 */
class Button extends \Personalwork\Forms\Elements
{
    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    /**
     * 建立一個按鈕標籤
     * */
    public function render($attr = NULL) {
        $attr['type'] = 'button';
        $attr['id']   = $this->getName();
        $attr['name'] = $this->getName();
        foreach ($this->getAttributes() as $key => $value) {
            $attr[$key] = $value;
        }

        $elem = "\t".Tag::tagHtml('button', $attr, FALSE, TRUE, TRUE).PHP_EOL;
        $elem.=$this->getLabel();
        $elem.="\t".Tag::tagHtmlClose('button').PHP_EOL;

        return $elem;
    }
}
?>