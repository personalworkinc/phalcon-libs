<?php
namespace Personalwork\Forms\People;

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

/**
 * Personalwork\Forms\People\RegisterForm
 */
class RegisterForm extends Form
{
    public function initialize($entity=null, $options=null)
    {
        $name = new Text('name', array('required'=>'required'));
        $name->setLabel('姓名');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => '姓名欄位必須填寫'
            ))
        ));
        $this->add($name);

        //Email
        $email = new Text('email', array('required'=>'required') );
        $email->setLabel('帳號');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Email欄位必須填寫'
            )),
            new Email(array(
                'message' => 'Email格式不符'
            ))
        ));
        $this->add($email);

        //Password
        $password = new Password('password', array('required'=>'required'));
        $password->setLabel('密碼');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => '密碼欄位必須填寫'
            )),
            new StringLength(array(
                'min' => 8,
                'messageMinimum' => '密碼欄位長度太少，請至少輸入8個字元'
            )),
            //欄位比對！
            new Confirmation(array(
                'message' => '兩次輸入密碼值不相符，請檢查',
                'with' => 'confirmPassword'
            ))
        ));
        $this->add($password);

        //Confirm Password
        $confirmPassword = new Password('confirmPassword', array('required'=>'required', 
                                                                 'data-validation-match-match'=>"password",
                                                                 'data-validation-match-message'=>"驗證密碼比對錯誤"));
        $confirmPassword->setLabel('Confirm Password');
        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => '驗證密碼欄位必須填寫'
            ))
        ));
        $this->add($confirmPassword);

        /*term of policy
        $terms = new Check('terms', array(
            'value' => 'yes'
        ));
        $terms->setLabel('Accept terms and conditions');
        // $terms->addValidator(
            // new Identical(array(
                // 'value' => 'yes',
                // 'message' => 'Terms and conditions must be accepted'
            // ))
        // );
        $this->add($terms);*/

        //CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                'message' => 'CSRF validation failed'
            ))
        );

        $this->add($csrf);

        //Sign Up
        $this->add(new Submit('Register', array(
            'class' => 'btn btn-sm btn-primary btn-red',
            'value' => '註冊'
        )));

    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}
