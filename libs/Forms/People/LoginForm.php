<?php
namespace Personalwork\Forms\People;

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Forms\Element\Select,
Phalcon\Forms\Element\Hidden,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical;

/**
 * Personalwork\Forms\People\LoginForm
 */
class LoginForm extends Form
{
    public function initialize()
    {
        //$translate = $this->getDi()->get('translate');

        //Email
        $email = new Text('email', array(
            //'placeholder' => $translate['Email']
            'placeholder' => 'Email',
            'class'       => 'form-control'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                //'message' => $translate['The e-mail is required']
                //'message' => 'The e-mail is required'
                'message' => '請輸入E-mail欄位'
            )),
            new Email(array(
                //'message' => $translate['The e-mail is not valid']
                //'message' => 'The e-mail is not valid'
                'message' => 'E-mail填寫格式錯誤，請檢查'
            ))
        ));

        $this->add($email);

        //Password
        $password = new Password('password', array(
            //'placeholder' => $translate['Password']
            'placeholder' => 'Password',
            'class'       => 'form-control'
        ));

        $password->addValidator(
            new PresenceOf(array(
                //'message' => $translate['The password is required']
                //'message' => 'The password is required'
                'message' => '請輸入密碼欄位'
            ))
        );
        $this->add($password);
        
 
        $role = new Select('portal', array(
            1 => '計畫入口',
            2 => '管理後台'
        ), array(
            'class' => 'form-control'
        ));
        $role->setLabel('選擇您的身份？');
        $this->add($role);
        

        /*Remember
        $remember = new Check('remember', array(
            'value' => 'yes'
        ));

        //$remember->setLabel($translate['Remember me']);
        $remember->setLabel('Remember me');

        $this->add($remember);*/

        //CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                //'message' => 'CSRF validation failed'
                'message' => '表單驗證碼比對錯誤！請重新處理'
            ))
        );

        $this->add($csrf);

        $this->add(new Submit('act', array(
            'value' => '登入',
            'class' => 'btn btn-sm btn-primary btn-red'
        )));
    }
}
