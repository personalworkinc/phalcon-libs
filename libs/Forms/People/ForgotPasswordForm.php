<?php
namespace Personalwork\Forms\People;

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Submit,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email;

/**
 * Personalwork\Forms\People\ForgotPasswordForm
 */
class ForgotPasswordForm extends Form
{
    public function initialize()
    {
        $email = new Text('email', array(
            'placeholder' => 'Email',
            'class'       => 'form-control'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                //'message' => 'The e-mail is required'
                'message' => '請輸入E-mail欄位'
            )),
            new Email(array(
                //'message' => 'The e-mail is not valid'
                'message' => 'E-mail填寫格式錯誤，請檢查'
            ))
        ));
        $this->add($email);
    
        //CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(
            new Identical(array(
                'value' => $this->security->getSessionToken(),
                //'message' => 'CSRF validation failed'
                'message' => '表單驗證碼比對錯誤！請重新處理'
            ))
        );

        $this->add($csrf);
        
        $this->add(new Submit('Send', array(
            'class' => 'btn btn-sm btn-primary btn-red',
            'value' => '送出'
        )));
    }
}
