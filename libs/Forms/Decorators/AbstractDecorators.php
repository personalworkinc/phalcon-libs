<?php
namespace Personalwork\Forms\Decorators;

use Phalcon\Tag as Tag;

/**
 * Description of FormAbstract
 * 1. 直接判制element是否有設定label來決定是否加入<label>
 * 2.
 * @author San Huang
 */
abstract class AbstractDecorators implements Ainterface {

    /**
     * Element
     *
     * @var Phalcon\Forms\Element\
     */
    protected $element;

    /**
     * HTML string
     *
     * @var string
     */
    protected $html = '';

    /**
     * Get Element
     *
     * @return Phalcon\Forms\Element\
     */
    public function getElement() {

        return $this->element;
    }

    /**
     * Set Element
     *
     * @param \Modules\View\Form\Phalcon\Forms\Element $element
     */
    public function setElement($element) {
        $this->element = $element;
    }

    /**
     * Generate label html
     *
     * @return void
     */
    public function generateLabel()
    {
        if(is_null($this->getElement()->getLabel()))
            return;

        $this->html.= "\t\t\t".Tag::tagHtml('label',array('for'=> $this->getElement()->getName() ), FALSE, TRUE, TRUE);
        $this->html.= $this->getElement()->getLabel();
        $this->html.= Tag::tagHtmlClose('label').PHP_EOL;
    }

    /**
     * Generate element html
     *
     * @return void
     */
    public function generateElement()
    {
        $this->html.= "\t\t\t".$this->element->render().PHP_EOL;
    }

    /**
     * Generate help-block html
     *
     * @return void
     */
    public function generateHelpBlock()
    {
        if( is_null($this->getElement()->getAttribute('description')) )
            return;

        $this->html.= "\t\t\t".Tag::tagHtml('p',array('class'=>'help-block'), FALSE, TRUE, TRUE);
        $this->html.= $this->getElement()->getAttribute('description');
        $this->html.= Tag::tagHtmlClose('p').PHP_EOL;

        $this->element->setAttribute('description','');
    }

    /**
     * Generate error list
     *
     * If you woudlike to view all error messages you shoud
     * overlow this message and create a loop for $error array
     *
     */
    public function generateErrors()
    {
        $errors = $this->element->getForm()->getMessagesFor($this->getElement()->getName());

        if($errors->count() > 0){
            $this->html.= "\t\t\t".Tag::tagHtml('ul', array(
                'class' => 'error'
            ), FALSE, TRUE, TRUE).PHP_EOL;
            $this->html.= "\t\t\t".Tag::tagHtml('li',array(), FALSE, TRUE, TRUE).PHP_EOL;
            $this->html.= "\t\t\t".$errors->offsetGet(0)->getMessage();
            $this->html.= "\t\t\t".Tag::tagHtmlClose('li').PHP_EOL;
            $this->html.= "\t\t\t".Tag::tagHtmlClose('ul').PHP_EOL;

        }
    }

    /**
     * Generate HTML
     *
     * @param string $html
     */
    public function toHtml() {
        $this->generateElement();
        return $this->html;
    }

}
