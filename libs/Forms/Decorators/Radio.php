<?php
/**
 * Default decorator for textarea
 *
 * @author San Haung
 */
namespace Personalwork\Forms\Decorators;

use Phalcon\Tag as Tag;

class Radio extends AbstractDecorators{


    /**
     * Generate element html
     *
     * @return void
     */
    public function generateElement()
    {
        $opts = $this->element->getUserOptions();
        foreach( $opts as $i => $option){
            $this->element->setAttribute('id', $this->element->getName().$i );
            $this->element->setAttribute('value',$option['value']);
            $this->html.= "\t\t\t".$this->element->render().' '.$option['label'].PHP_EOL;
        }
    }

    /**
     * Generate HTML
     *
     * @param string $html
     */
    public function toHtml() {

        $this->html.=  "\t".Tag::tagHtml('div').PHP_EOL;

        $this->generateLabel();
        $this->generateElement();
        $this->generateHelpBlock();
        $this->generateErrors();

        $this->html.=  "\t".Tag::tagHtmlClose('div').PHP_EOL;;

        return $this->html;
    }
}
