<?php

/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 *
 */

namespace Personalwork\Forms\Decorators;


/**
 * 配置介面讓後續專案配置表單元素結構樣式可以參考
 * @author San Huang
 */
interface Ainterface {

    /*
     * Set Phalcon\Forms\Element\
     */
    public function setElement($element);

    /*
     * Get Phalcon\Forms\Element\
     */
    public function getElement();

    /*
     * Generate $html
     */
    public function toHtml();
}
