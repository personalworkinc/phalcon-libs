<?php
/**
 * Default decorator for hidden
 *
 * @author San Haung
 */
namespace Personalwork\Forms\Decorators;

use Phalcon\Tag as Tag;

class Submit extends AbstractDecorators{

    /**
     * Generate HTML
     *
     * @param string $html
     */
    public function toHtml() {

        $this->html.=  "\t".Tag::tagHtml('div', array('class'=>'form-group text-center')).PHP_EOL;

        $this->generateElement();
        $this->generateErrors();

        $this->html.=  "\t".Tag::tagHtmlClose('div').PHP_EOL;;

        return $this->html;
    }
}
