<?php
namespace Personalwork\Forms\Decorators;

use Phalcon\Tag as Tag;

/**
 * @author San Huang
 */
abstract class AbstractGroupDecorators {

    /**
     * Element
     *
     * @var Phalcon\Forms\Element\
     */
    protected $elements;

    /**
     * HTML string
     *
     * @var string
     */
    protected $html = '';


    public function __construct($elements){
        $this->setElements($elements);
    }

    /**
     * Get Element
     *
     * @return Phalcon\Forms\Element\
     */
    public function getElements() {
        return $this->element;
    }

    /**
     * Set Element
     *
     * @param \Modules\View\Form\Phalcon\Forms\Element $element
     */
    public function setElement($element) {
        $this->elements[] = $element;
        return $this;
    }

    /**
     * Set Elements
     *
     * @param mixed $elements[\Modules\View\Form\Phalcon\Forms\Element $element]
     */
    public function setElements($elements) {
        $this->elements = $elements;
        return $this;
    }

    /**
     * Generate HTML
     *
     * @param string $html
     */
    public function toHtml() {
        return $this->html;
    }
}