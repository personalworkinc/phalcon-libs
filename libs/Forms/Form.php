<?php

/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 *
 */
namespace Personalwork\Forms;

/**
 * Description of Form
 *
 * @author San Huang
 */
class Form extends \Phalcon\Forms\Form
{
   /**
     * Form http method
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Form action url
     *
     * @var string
     */
    protected $action;

    /**
     * Css class
     *
     * @var type
     */
    protected $cssClass;

    /**
     * CssId
     *
     * @var type
     */
    protected $cssId;

    /**
     * Form enctype
     *
     * @var type
     */
    protected $enctype;

    /**
     * Form custom user tags
     *
     * @var array
     */
    protected $customTags = array();

    /**
     * Get current form method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get css class
     *
     * @return string | null
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * Get css id
     *
     * @return string | null
     */
    public function getCssId()
    {
        return $this->cssId;
    }

    /**
     * Get form Enctype
     *
     * @return string | null
     */
    public function getEnctype()
    {
        return $this->enctype;
    }

    /**
     * Get form Enctype
     *
     * @return array
     */
    public function getCustomTags()
    {
        return $this->customTags;
    }

    /**
     * Set form action
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $method = strtoupper($method);

        if (in_array($method, array('POST', 'GET'))) {
            $this->method = $method;
        }

        return $this;
    }

    /**
     * Set form action
     *
     * @param type $action
     * @return \Modules\Form
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Set css Id
     *
     * @param type $action
     * @return \Modules\Form
     */
    public function setCssId($cssId)
    {
        $this->cssId = $cssId;

        return $this;
    }

    /**
     * Set css class
     *
     * @param type $action
     * @return \Modules\Form
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Set form enctype
     *
     * @param type $enctype
     * @return \Modules\Form
     */
    public function setEnctype($enctype)
    {
        $this->enctype = $enctype;

        return $this;
    }

    /**
     * Set custom tag
     *
     * @param type $customTags
     * @return \Modules\Form
     */
    public function addCustomTag($key, $value)
    {
        $this->customTags[$key] = $value;

        return $this;
    }

    /**
     * Render all Form
     */
    public function toHtml()
    {
         $helper = new \Personalwork\Mvc\View\Helpers\Form();
         return $helper->toHtml($this);
    }


    /**
     * 處理單一元素html結構
     *
     * @param String element's Name
     * @param Array assign options
     *
     * @return String html
     * */
    public function render($name, $opt=array()){
        if( isset($opt['decorator']) && class_exists($opt['decorator']) ){
            $elm_decorator = new $opt['decorator']($this->get($name));
            return $elm_decorator->toHtml();
        }elseif( $decorator=$this->get($name)->getDecorator() ){
            die( $decorator );
            $elm_decorator = new $decorator($this->get($name));
            return $elm_decorator->toHtml();
        }else{
            return $this->get($name)->render();
        }
    }


    /**
     * 處理集合元素html結構
     * 直接配置指定Decorator來呈現區塊元素集合
     *
     * @param Array element's Name
     * @param String decorator's ClassName
     *
     * @return String html
     * */
    public function renderGroups($elms=array(), $decorator){
        foreach ($elms as $elmName) {
            $elmObj[] = $this->get($elmName);
        }
        $decorators = new $decorator($elmObj);
        return $decorators->toHtml();
    }
}
