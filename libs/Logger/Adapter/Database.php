<?php
/**
 * Phalcon\Logger\Adapter\Database
 * Adapter to store logs in a database table
 */
/**
 * Personalwork\Logger\Adapter
 * @author  San Huang <san@mail.personalwork.tw>
 */

namespace Personalwork\Logger\Adapter;

use Phalcon\Logger\Exception;

/**
 * 訊息記錄器機制，透過預設綁定logger與people資料表
 * 直接使用$di物件取得logger紀錄器進行紀錄功能
 * 在 service.php 內
 * $di->set('logger', function () use ($di) {

        $logger = new \Personalwork\Logger\Adapter\Database(array(
                        'db'    => $di->get('db'),
                        'table' => "logger",
                        'name'  => 'info',
                        'router'=> $di->get('router'),
                        'request'=> $di->get('request'),
                        'session'=> $di->get('session'),
                      ));

        return $logger;
    });
 *
 * 在 controller.php的action 內
 * 登入紀錄
 * $this->logger->setEvent(\Personalwork\Logger\Adapter\Database::GLOBAL_AUTH_SUCCESS)
                     ->info('帳號'.$people->account.'於系統預設介面進行登出動作結果成功');
 * 登出紀錄
 * $this->logger->setEvent(\Personalwork\Logger\Adapter\Database::GLOBAL_AUTH_LOGOUT)
                     ->info('帳號'.$this->session->get("auth-identity")['account'].'於系統預設介面進行登出動作結果成功');
 *
 * 重點在於呼叫setEvent後接續呼叫不同警示等級類型方法
 * */
class Database extends \Phalcon\Logger\Adapter
               implements \Phalcon\Logger\AdapterInterface
{
    //\Phalcon\Logger::SPECIAL 9
    //\Phalcon\Logger::CUSTOM 8
    //\Phalcon\Logger::DEBUG 7
    //\Phalcon\Logger::INFO 6
    //\Phalcon\Logger::NOTICE 5
    //\Phalcon\Logger::WARNING 4
    //\Phalcon\Logger::ERROR 3
    //\Phalcon\Logger::ALERT 2
    //\Phalcon\Logger::CRITICAL 1
    //\Phalcon\Logger::EMERGENCE or EMERGENCY 0

    /**
     * 定義事件名稱
     */
    /**
     * 通用事件：帳號驗證，登入驗證成功
     */
    const GLOBAL_AUTH_SUCCESS = 10;
    /**
     * 通用事件：帳號驗證，登入驗證失敗
     */
    const GLOBAL_AUTH_FAIL = 11;
    /**
     * 通用事件：帳號驗證，正常登出
     */
    const GLOBAL_AUTH_LOGOUT = 12;

    /**
     * 通用事件：系統事件，定期排程
     */
    const EVENT_SYSTEM_CRON = 20;
    /**
     * 通用事件：系統事件，訊息通知
     */
    const EVENT_SYSTEM_NOTIFICATION = 21;

    /**
     * 通用事件：物件新增
     */
    const GLOBAL_OPERATE_CREATE = 31;

    /**
     * 通用事件：物件修改
     */
    const GLOBAL_OPERATE_UPDATE = 32;

    /**
     * 通用事件：物件移除
     */
    const GLOBAL_OPERATE_REMOVE = 33;

    /**
     * 通用事件：物件刪除
     */
    const GLOBAL_OPERATE_DELETE = 34;

    /**
     * 通用事件：資料匯入
     */
    const GLOBAL_OPERATE_IMPORT = 35;

    /**
     * 通用事件：資料匯出
     */
    const GLOBAL_OPERATE_EXPORT = 36;

    /**
     * 通用事件：資料庫操作發生錯誤
     */
    const GLOBAL_DATABASE_ERROR = 40;

    /**
     * 通用事件：資料庫操作紀錄查詢
     */
    const GLOBAL_DATABASE_FETCH = 41;


    /**
     * 定義觸發器名稱
     */
    const TRIGGER_DEFAULT = '[system]';
    //進入頁面[特定Action]
    const TRIGGER_ROUTER = '[router]';
    //由外掛、第三方函式庫或其他程式碼觸發
    const TRIGGER_CODE = '[code]';


    protected $lvlname = array(
                            '緊急事件',
                            '危及事件',
                            '警示事件',
                            '錯誤事件',
                            '警告事件',
                            '注意事件',
                            '一般記錄',
                            '詳細資訊',
                            '自訂訊息',
                            '特別訊息'
                         );

    /**
     * Name
     */
    protected $name;

    /**
     * Adapter options
     */
    protected $options;


    private $pettern = '';

    /**
     * Class constructor.
     *
     * @param  string                    $name
     * @param  array                     $options
     * @throws \Phalcon\Logger\Exception
     */
    public function __construct($options = array())
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }

        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }

        $this->options = $options;

        /**
         * 通用事件：預設事件代碼
         */
        $this->event = 0;
        //預設觸發器
        $this->trigger = self::TRIGGER_ROUTER.$this->options['router']->getRewriteUri();;
        //預設變數
        $this->vars = serialize($this->options['request']->getPost());

        // 配置Session
        if( isset($this->options['session']) ){
        $this->session = $this->options['session'];
        }
    }


    /**
     * 配置事件代碼
     */
    public function setEvent($e) {
        $this->event = $e;
        return $this;
    }


    /**
     * 預設使用路由
     */
    public function setTrigger($t, $prefix='[router]') {
        if(empty($t)){
            $this->trigger = $prefix.$t;
        }
        return $this;
    }


    public function setVars(array $vars=null){
        $this->vars = serialize($vars);
        return $this;
    }

    /**
     * Writes the log to the Database
     */
    public function debug($message, array $context=null){
        //設定特殊紀錄格式

        return $this->logInternal($message, \Phalcon\Logger::DEBUG, time(), $context);
    }

    public function info($message, array $context=null){
        //設定特殊紀錄格式

        return $this->logInternal($message, \Phalcon\Logger::INFO, time(), $context);
    }

    /**
     * pettern : %who%[於%location%]進行%act%動作結果%result%[造成%append%]
     * @param $match array('origin', 'who', 'ignore', 'location', 'act', 'result', 'ignore', 'append');
     */
    protected function _pettern($msg){
        preg_match("/([^於]+)(於(.+))*進行(.+)動作結果([^造|。]+)(造成([^。]+))?/", $msg, $match);

        // var_dump($match);
        if( count($match) > 0){
            $msg = str_replace($match[1], "[%who%{$match[1]}]", $msg);
            if( !empty($match[3]) ){
                $msg = str_replace('於'.$match[3], "於[%location%{$match[3]}]", $msg);
            }
            $msg = str_replace('進行'.$match[4], "進行[%act%{$match[4]}]", $msg);
            $msg = str_replace($match[5], "[%result%{$match[5]}]", $msg);
            if( !empty($match[7]) ){
                $msg = str_replace($match[7], "[%append%{$match[7]}]", $msg);
            }
            return $msg;
        }else{
            return false;
        }
    }

    public function notice($message, array $context=null){
        //設定特殊紀錄格式
        return $this->logInternal($message, \Phalcon\Logger::NOTICE, time(), $context);
    }

    public function warning($message, array $context=null){
        //設定特殊紀錄格式
        return $this->logInternal($message, \Phalcon\Logger::WARNING, time(), $context);
    }

    public function error($message, array $context=null){
        //設定特殊紀錄格式

        return $this->logInternal($message, \Phalcon\Logger::ERROR, time(), $context);
    }

    public function critical($message, array $context=null){
        //設定特殊紀錄格式
        return $this->logInternal($message, \Phalcon\Logger::CRITICAL, time(), $context);
    }

    public function alert($message, array $context=null){
        //設定特殊紀錄格式
        return $this->logInternal($message, \Phalcon\Logger::ALERT, time(), $context);
    }

    public function emergency($message, array $context=null){
        //設定特殊紀錄格式
        return $this->logInternal($message, \Phalcon\Logger::EMERGENCY, time(), $context);
    }


    /**
     * Writes the log to the file itself
     *
     * @param string  $message
     * @param integer $type
     * @param integer $time
     * @param array   $context
     */
    public function logInternal($message, $type = null, $time, $context = array())
    {
        if( empty($message) ){
            throw new \Phalcon\Exception(__FILE__ . "： 未設定記錄訊息！");
        }elseif( !($pettern_msg=$this->_pettern($message)) ){
            $phql = "INSERT INTO {$this->options['table']}
                 (`loggerId`, `type`, `event`, `trigger`, `message`, `setTime`, `user_agent`, `vars`)
                 VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)";

            $this->options['db']->execute($phql, array(\Phalcon\Logger::ALERT,
                                                       self::EVENT_SYSTEM_NOTIFICATION,
                                                       self::TRIGGER_CODE.'Personalwork\Logger\Adapter\Database',
                                                       '本記錄訊息無法被格式化，請檢查！',
                                                       $time,
                                                       null,
                                                       serialize( $message ) )
            );
            return false;
        }

        $phql = "INSERT INTO {$this->options['table']}
                 (`loggerId`, `type`, `event`, `trigger`, `message`, `setTime`, `user_agent`, `vars`, `PeopleId`)
                 VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?)";

        $user_agent = ( is_null($context['user_agent']) ) ? $_SERVER['HTTP_USER_AGENT'] : $context['user_agent'] ;

        $vars = ( is_null($context['vars']) ) ? $this->vars : serialize( $context['vars'] );

        $peopleId = ( isset($this->session) ) ? $this->session->get('auth-identity')->employeeId : null ;

        return $this->options['db']->execute($phql, array($type,
                                                          $this->event,
                                                          $this->trigger,
                                                          // 格式化訊息
                                                          $pettern_msg,
                                                          $time,
                                                          $user_agent,
                                                          $this->vars,
                                                          // 2015-12-04 新增關聯使用者編號
                                                          $peopleId)
               );
    }


    /**
     * {@inheritdoc}
     * 訊息樣板
     * @return \Phalcon\Logger\Formatter\Line
     */
    public function getFormatter()
    {
        $pettern = '[%who%]{於}[%location%]{對}[%target%]{進行}[%act%動作]{結果}[%result%成功或失敗]{造成}[%append%導致]';
    }


    /**
     * {@inheritdoc}
     *
     * @return boolean
     */
    public function close()
    {
        $this->options['db']->close();
    }
}