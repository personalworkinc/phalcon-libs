--
-- Table structure for table `people`
-- created from project mcntu
-- updated from project cyber_tenread
--
CREATE TABLE `people` (
  `peopleId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(20) DEFAULT '' COMMENT '姓名',
  `account` varchar(30) NOT NULL DEFAULT '' COMMENT '帳號(電子郵件或手機)',
  `alias` varchar(50) DEFAULT '' COMMENT '識別碼',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '郵件信箱',
  `password` varchar(120) NOT NULL DEFAULT '' COMMENT '密碼',
  `RoleId` tinyint(2) DEFAULT NULL COMMENT '群組編號',
  `RoleLabel` varchar(20) DEFAULT '' COMMENT '群組名稱',
  `statecode` tinyint(1) NOT NULL DEFAULT '0' COMMENT '最新狀態',
  `initTime` int(11) unsigned NOT NULL COMMENT '帳號註冊時間',
  `lastTime` int(11) unsigned DEFAULT NULL COMMENT '最後登入時間',
  PRIMARY KEY (`peopleId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='帳號資料表';

-- 設定第一個管理者帳號
INSERT INTO `people` (`peopleId`, `name`, `account`, `alias`, `email`, `password`, `RoleId`, `RoleLabel`, `statecode`, `initTime`, `lastTime`)
VALUES
  (1, '管理者', 'admin', 'admin', 'admin@tenread.hstestweb.com', '$2y$08$UWdOWnZpS1V6c0llcEowLuv8G5APSLZ/V7.uLZ0RG0mKmcRXMxbYS', 1, 'admin', 1, 0, NULL);


--
-- Table structure for table `people_extendinfo`
-- created from project mcntu
-- updated from project cyber_tenread
--
CREATE TABLE `people_extendinfo` (
  `extendinfoId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `PeopleId` int(11) unsigned NOT NULL COMMENT '關聯會員編號',
  `key` varchar(30) NOT NULL DEFAULT '0' COMMENT '鍵值',
  `value` varchar(10) NOT NULL DEFAULT '0' COMMENT '數值',
  PRIMARY KEY (`extendinfoId`),
  KEY `PeopleId` (`PeopleId`),
  CONSTRAINT `fk_people_extendinfo_1` FOREIGN KEY (`PeopleId`) REFERENCES `people` (`peopleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='帳號延伸資訊表';

--
-- Table structure for table `logger`
-- created from project mcntu
-- updated from project pkerp
-- updated from project cyber_tenread
--
CREATE TABLE `logger` (
  `loggerId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `type` tinyint(1) NOT NULL COMMENT '訊息類型',
  `event` varchar(50) NOT NULL DEFAULT '' COMMENT '事件名稱/發生原因代碼',
  `trigger` varchar(255) NOT NULL DEFAULT '' COMMENT '觸發/動作(route or actoin or event trigger)',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '訊息',
  `setTime` int(10) unsigned NOT NULL COMMENT '記錄時間',
  `user_agent` varchar(512) DEFAULT NULL COMMENT '記錄使用裝置',
  `vars` text COMMENT '事件參數',
  `PeopleId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`loggerId`),
  KEY `fk_logger1` (`PeopleId`),
  CONSTRAINT `fk_logger_peopleId` FOREIGN KEY (`PeopleId`) REFERENCES `people` (`peopleId`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='訊息記錄表';